export default function ($http)
{	
	'ngInject';
		
	const MyStats = {};

	MyStats.totalDistance = () => $http.get('/api/mystats/totaldistance');
	MyStats.totalTime = () => $http.get('/api/mystats/totaltime');

	return MyStats;
}