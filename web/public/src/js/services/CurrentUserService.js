export default function ($window)
{	
	'ngInject';
		
	const CurrentUserService = {};

	CurrentUserService.user = null;

	CurrentUserService.setUser = (user, tokens) =>
	{	
		if (user === undefined)
		{
			throw new Error('setUser requires a valid user object');
			return false;
		}

		CurrentUserService.user = {};

		CurrentUserService.user.userid   = user.userid;
		CurrentUserService.user.username = user.username;
		CurrentUserService.user.isAdmin  = user.isAdmin;

		if (tokens !== undefined)
		{
			$window.sessionStorage.accessToken = tokens.access;
			$window.sessionStorage.refreshToken = tokens.refresh;
		}
	}

	CurrentUserService.getUser = () =>
	{
		return (CurrentUserService.user !== null)? CurrentUserService.user : false;
	}

	CurrentUserService.destroy = () =>
	{
		CurrentUserService.user = null;

		delete $window.sessionStorage.accessToken;
		delete $window.sessionStorage.refreshToken;
	}

	return CurrentUserService;
}

