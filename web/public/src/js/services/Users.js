export default function ($http)
{	
	'ngInject';
		
	const Users = {};

	Users.retrieveUsers = () => $http.get('/api/users');
	Users.deleteUser = (userID) => $http.delete('/api/users/' + userID);
	Users.retrieveUser = (userID) => $http.get('/api/users/' + userID);
	Users.updateUser = (userID, data) => $http.put('/api/users/' + userID, data);
	Users.updatePassword = (userID, newPassword) => $http.put('/api/users/' + userID + '/password', {newPassword: newPassword});

	return Users;
}