export default function ($http)
{	
	'ngInject';
		
	const Runs = {};

	Runs.retrieveRuns = () => $http.get('/api/runs');
	Runs.deleteRun= (runID) => $http.delete('/api/runs/' + runID);
	Runs.retrieveRun = (runID) => $http.get('/api/runs/' + runID);
	Runs.retrieveAllRuns = () => $http.get('/api/runs/all');
	Runs.retrieveUserRuns = (runID) => $http.get('/api/runs/all/' + runID);

	return Runs;
}