export default function ($rootScope)
{	
	'ngInject';
		
	const feedbackService = {};

	$rootScope.$on('$routeChangeSuccess', () =>
	{
		feedbackService.resetMessage();
	});

	feedbackService.message = {text : null, state : null};

	feedbackService.resetMessage = () =>
	{
		feedbackService.message.state = null;
		feedbackService.message.text  = null;
	}

	feedbackService.showMessage = (state, text) =>
	{
		if (state === undefined)
		{
			throw new Error('Please define a state for a feedback message');
		}

		if (text === undefined)
		{
			throw new Error('Please enter the message for a feedback alert');
		}

		feedbackService.message.state = state;
		feedbackService.message.text  = text;
	}

	return feedbackService;
}