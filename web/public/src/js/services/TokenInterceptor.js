export default function ($q, $rootScope, $window, $location, $timeout, $injector, CurrentUser)
{	
	'ngInject';
		
	const tokenInterceptor = {};

	let $http;

	$timeout(() =>
	{
		$http = $injector.get('$http');
	});

	tokenInterceptor.request = (config) =>
	{
		config.headers = config.headers || {};

		if ($window.sessionStorage.accessToken)
		{
			config.headers.Authorization = 'Bearer ' + $window.sessionStorage.accessToken;
		}

		return config;
	}

	tokenInterceptor.responseError = (rejection) =>
	{
		if (rejection != null && rejection.status === 401 && ($window.sessionStorage.accessToken || CurrentUser.user))
		{
			CurrentUser.destroy();
			$location.path('/login');
			$rootScope.$broadcast('auth-not-authenticated');
		}

		if (rejection != null && rejection.status === 403 && ($window.sessionStorage.accessToken || CurrentUser.user))
		{	
			$location.path('/');
			$rootScope.$broadcast('auth-not-authorized');
		}

		if (rejection != null && rejection.status === 500)
		{
			console.log('tokenInterceptor.js - 500 error handling');
		}	

		return $q.reject(rejection);
	}

	return tokenInterceptor;
}