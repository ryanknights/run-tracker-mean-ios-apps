export default function ($rootScope, $http, CurrentUser, $q)
{	
	'ngInject';
		
	const authService = {};

	authService.userIsReauthenticating = false;

	authService.isLoggedIn = () =>
	{
		return (CurrentUser.user !== null);
	}

	authService.isAdmin = () =>
	{	
		if (!CurrentUser.user)
		{
			return false;
		}
		
		return (CurrentUser.user.isAdmin === true);
	}

	authService.register = (username, email, password) => $http.post('/api/register', {username : username, email : email, password : password});

	authService.logIn = (username, password) =>
	{	
		let deferred = $q.defer();

		$http.post('/api/login', {username : username, password : password})

			.then((res) =>
			{
				CurrentUser.setUser(res.data.user, res.data.token);
				
				$rootScope.$broadcast('auth-login-success');

				deferred.resolve();

			}, () =>
			{
				$rootScope.$broadcast('auth-login-failed');

				deferred.reject();
			});

		return deferred.promise;
	}

	authService.logOut = () =>
	{
		authService.userIsReauthenticating = false;

		CurrentUser.destroy();

		$rootScope.$broadcast('auth-logout-success');
	}	

	authService.reAuthenticate = () =>
	{
		return $http.get('/api/authenticate')

				.then((res) =>
				{
					CurrentUser.setUser(res.data.user);

					authService.userIsReauthenticating = false;

					$rootScope.$broadcast('auth-login-reauthenticate-success');

					return res.data.user;

				}, () =>
				{	
					$rootScope.$broadcast('auth-login-reauthenticate-failed');

					return null;
				});
	}

	return authService;
}

