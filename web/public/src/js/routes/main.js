export default function ($routeProvider, $locationProvider)
{	
	'ngInject';

	import { runs, run, users, user, allRuns, userRuns } from './resolves';
		
	$routeProvider

		.when('/',
		{	
			templateUrl  : '/src/js/views/home.html',
			controller   : 'homeCtrl',
			controllerAs : 'home',
			access       : { requiredLogin: true } 
		})
		.when('/runs',
		{	
			templateUrl  : '/src/js/views/runs.html',
			controller   : 'runsCtrl',
			controllerAs : 'runs',
			access       : { requiredLogin: true },
			resolve      : { runs: runs }
		})
		.when('/runs/:id',
		{	
			templateUrl  : '/src/js/views/run.html',
			controller   : 'runCtrl',
			controllerAs : 'run',
			access       : { requiredLogin: true },
			resolve      : { run: run }
		})
		.when('/users-runs',
		{	
			templateUrl  : '/src/js/views/users-runs.html',
			controller   : 'usersRunsCtrl',
			controllerAs : 'usersRuns',
			access       : { requiredLogin: true, isAdmin: true },
			resolve      : { allRuns: allRuns }
		})
		.when('/users-runs/:id',
		{	
			templateUrl  : '/src/js/views/user-runs.html',
			controller   : 'userRunsCtrl',
			controllerAs : 'userRuns',
			access       : { requiredLogin: true, isAdmin: true },
			resolve      : { userRuns: userRuns }
		})		
		.when('/users',
		{	
			templateUrl  : '/src/js/views/users.html',
			controller   : 'usersCtrl',
			controllerAs : 'users',
			access       : { requiredLogin: true },
			resolve      : { users: users }
		})
		.when('/users/:id',
		{
			templateUrl : 'src/js/views/user.html',
			controller  : 'userCtrl',
			controllerAs : 'user',
			access       : { requiredLogin: true },
			resolve      : { user: user }
		})					
		.when('/login',
		{	
			templateUrl  : '/src/js/views/login.html',
			controller   : 'loginCtrl',
			controllerAs : 'login',
			access       : { requiredLogin: false } 
		})
		.when('/register',
		{	
			templateUrl  : '/src/js/views/register.html',
			controller   : 'registerCtrl',
			controllerAs : 'register',
			access       : { requiredLogin: false }
		})		
}