/*----------  Resolve Admin Users  ----------*/

export function users ($q, Users)
{
	return Users.retrieveUsers().then((res) =>
	{
		return res.data.users;

	}, (res) =>
	{
		return $q.reject(res);
	});
}

/*----------  Resolve Admin User  ----------*/

export function user ($q, $route, Users)
{
	return Users.retrieveUser($route.current.params.id).then((res) =>
	{
		return res.data.user;

	}, (res) =>
	{
		return $q.reject(res);
	});	
}

/*----------  Resolve Runs  ----------*/

export function runs ($q, Runs)
{
	return Runs.retrieveRuns().then((res) =>
	{
		return res.data.runs;

	}, (res) =>
	{
		return $q.reject(res);
	});
}


/*----------  Resolve Run  ----------*/

export function run ($q, $route, Runs)
{
	return Runs.retrieveRun($route.current.params.id).then((res) =>
	{
		return res.data.run;

	}, (res) =>
	{
		return $q.reject(res);
	});
}

/*----------  All Runs  ----------*/

export function allRuns ($q, Runs)
{
	return Runs.retrieveAllRuns().then((res) =>
	{
		return res.data.runs;

	}, (res) =>
	{
		return $q.reject(res);
	});
}

/*----------  User Runs  ----------*/

export function userRuns ($q, $route, Runs)
{
	return Runs.retrieveUserRuns($route.current.params.id).then((res) =>
	{
		return res.data.runs;

	}, (res) =>
	{
		return $q.reject(res);
	});
}
