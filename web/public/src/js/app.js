import Config from './modules/config';
import Controllers from './modules/controllers';
import Routes from './modules/routes';
import Services from './modules/services';
import Directives from './modules/directives';
import Filters from './modules/filters';

angular.module('run-tracker', ['ngRoute', 'chart.js', Config.name, Controllers.name, Routes.name, Services.name, Directives.name, Filters.name]);