import feedbackMessage from '../directives/feedbackMessage';
import runMap from '../directives/runMap';

const module = angular.module('run-tracker.directives', [])
	.directive('feedbackMessage', feedbackMessage)
	.directive('runMap', runMap);

export default module;