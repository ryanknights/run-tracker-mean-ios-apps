import mainRoutes from '../routes/main';

const module = angular.module('runTracker.routes', [])
	.config(mainRoutes);

export default module;