import distanceType from '../filters/distanceType';
import secondsToTime from '../filters/secondsToTime';
import paceType from '../filters/paceType';

const module = angular.module('run-tracker.filters', [])
	.filter('distanceType', distanceType)
	.filter('secondsToTime', secondsToTime)
	.filter('paceType', paceType);

export default module;