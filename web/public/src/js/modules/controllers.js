import applicationCtrl from '../controllers/applicationCtrl';
import homeCtrl from '../controllers/homeCtrl';
import runsCtrl from '../controllers/runsCtrl';
import runCtrl from '../controllers/runCtrl';
import loginCtrl from '../controllers/loginCtrl';
import registerCtrl from '../controllers/registerCtrl';
import usersCtrl from '../controllers/usersCtrl';
import userCtrl from '../controllers/userCtrl';
import usersRunsCtrl from '../controllers/usersRunsCtrl';
import userRunsCtrl from '../controllers/userRunsCtrl';

const module = angular.module('run-tracker.controllers', [])
	.controller('applicationCtrl', applicationCtrl)
	.controller('homeCtrl', homeCtrl)
	.controller('runsCtrl', runsCtrl)
	.controller('runCtrl', runCtrl)
	.controller('loginCtrl', loginCtrl)
	.controller('registerCtrl', registerCtrl)
	.controller('usersCtrl', usersCtrl)
	.controller('userCtrl', userCtrl)
	.controller('usersRunsCtrl', usersRunsCtrl)
	.controller('userRunsCtrl', userRunsCtrl);	

export default module;