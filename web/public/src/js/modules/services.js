import authService from '../services/AuthService';
import currentUserService from '../services/CurrentUserService';
import tokenInterceptor from '../services/TokenInterceptor';
import feedbackService from '../services/Feedback';
import runsService from '../services/Runs';
import usersService from '../services/Users';
import myStatsService from '../services/MyStats';

const module = angular.module('gps-tracker.services', [])
	.factory('Auth', authService)
	.factory('CurrentUser', currentUserService)
	.factory('TokenInterceptor', tokenInterceptor)
	.factory('Feedback', feedbackService)
	.factory('Runs', runsService)
	.factory('Users', usersService)
	.factory('MyStats', myStatsService);

export default module;