export default function ()
{	
	'ngInject';
		
	return {

		restrict         : 'AE',
		replace          : true,
		templateUrl      : '/src/js/views/directive-runMap.html',
		scope            : 
		{
			locations : '='
		},
		bindToController : true,
		controller ($scope, $element)
		{
			var vm = this;

			if (vm.locations === undefined)
			{
				vm.locations = [];
			}

			let map,
				markers = [],
				bounds  = new google.maps.LatLngBounds(),
				infoWindow = new google.maps.InfoWindow();

			function initMap ()
			{
				map = new google.maps.Map($element[0]);

				if (vm.locations.length)
				{	
					addMarker(vm.locations[0]);
					addMarker(vm.locations[vm.locations.length - 1]);					
					addPolyline();
					fitBounds();
				}			
			}

			function addPolyline()
			{
			  	for (let i = 0, l = vm.locations.length; i < l; i++)
			  	{
			  		if (i === 0) continue;

			  		let beginLineCoords = {lat: vm.locations[i - 1].location.lat, lng: vm.locations[i - 1].location.lng},
			  			endLineCoords   = {lat: vm.locations[i].location.lat, lng: vm.locations[i].location.lng},
			  			coords          = [beginLineCoords, endLineCoords];

					let runPath = new google.maps.Polyline({
						path          : coords,
						geodesic      : true,
						strokeColor   : '#'+(Math.random()*0xFFFFFF<<0).toString(16),
						strokeOpacity : 1.0,
						strokeWeight  : 2
					});

					google.maps.event.addListener(runPath, 'click', function ()
					{
						infoWindow.setContent(vm.locations[i].description);
						infoWindow.open(map, marker);
					});						

					runPath.setMap(map);
			  	}

				// vm.locations.forEach((data) =>
				// {	
				// 	coords.push({lat: data.location.lat, lng: data.location.lng})
				// });

				// var runPath = new google.maps.Polyline({
				// 	path          : coords,
				// 	geodesic      : true,
				// 	strokeColor   : '#FF0000',
				// 	strokeOpacity : 1.0,
				// 	strokeWeight  : 2
				// });

				// runPath.setMap(map);
			}

			function addMarker(data)
			{
				let marker = new google.maps.Marker(
				{
					position  : new google.maps.LatLng(data.location.lat, data.location.lng),
					map       : map
				});

				google.maps.event.addListener(marker, 'click', function ()
				{
					infoWindow.setContent(data.description);
					infoWindow.open(map, marker);
				});				
			}

			function fitBounds()
			{
				let bounds = new google.maps.LatLngBounds();

				vm.locations.forEach((data) =>
				{
					let locationLatLng = new google.maps.LatLng(data.location.lat, data.location.lng);

					bounds.extend(locationLatLng);
				});

				map.fitBounds(bounds);
			}
			
			initMap();
		},
		controllerAs : 'vm'
	}
}