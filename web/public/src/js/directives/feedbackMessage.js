export default function ()
{	
	'ngInject';
		
	return {

		restrict         : 'AE',
		replace          : true,
		templateUrl      : '/src/js/views/directive-feedbackMessage.html',
		scope            : {},	
		controller (Feedback)
		{	
			var vm = this;

			vm.message = Feedback.message;

			vm.closeMessage = () => Feedback.resetMessage();
		},
		controllerAs : 'vm'
	}
}