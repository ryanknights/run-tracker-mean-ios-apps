export default function ()
{	
	'ngInject';
		
	return (input, type) =>
	{	
		console.log(input);
		
		if (!input)
		{
			return false;
		}

		let distance;

		switch (type)
		{
			case 'meters':
				distance = input;
			break;

			case 'km':
				distance = input / 1000;
			break;

			case 'miles':
				distance = input / 1609.344;
			break;
		}

		return distance.toFixed(2);
	}
}