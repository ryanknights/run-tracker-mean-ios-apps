export default function ()
{	
	'ngInject';
		
	return (seconds, meters, type) =>
	{	
		if (!seconds)
		{
			return false;
		}
		
		let pace;

		switch (type)
		{
			case 'mpm':
				var minutes = seconds / 60,
					miles   = meters / 1609.344;

				pace = minutes / miles;
			break;

			case 'mph':
				var hours = seconds / 3600,
					miles = meters / 1609.344;

				pace = miles / hours;
			break;

			case 'km/h':
				var hours = seconds / 3600,
					km = meters / 1000;

				pace = km / hours;
			break;
		}

		return pace.toFixed(1);
	}
}