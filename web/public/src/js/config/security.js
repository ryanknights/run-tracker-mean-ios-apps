export default ($rootScope, $location, $window, Auth) =>
{	
	'ngInject';

	function preventNotAuthenticated (event)
	{
    	event.preventDefault();

		$rootScope.$broadcast('auth-not-authenticated');  

		$location.path('/login');

		Auth.userIsReauthenticating = false;
	}

	function preventNotAuthorized (event)
	{
    	$rootScope.$broadcast('auth-not-authorized');

    	$location.path('/');		
	}
	
	if (window.sessionStorage.accessToken || window.sessionStorage.refreshToken) // If we have a token stored we assume a user has been logged in
	{	
		Auth.userIsReauthenticating = true;
	}

	$rootScope.$on('$routeChangeStart', (event, nextRoute, currentRoute) =>
	{
	    if (nextRoute.access && nextRoute.access.requiredLogin && !Auth.isLoggedIn()) 
	    {
	    	if (Auth.userIsReauthenticating)
	    	{
	    		Auth.reAuthenticate().then((user) =>
    			{
    				if (!user)
    				{
    					return preventNotAuthenticated(event);
    				}
    			});
	    	}
	    	else
	    	{
	    		return preventNotAuthenticated(event);
	    	}
	    }
	});
}