export default ($httpProvider) =>
{
	'ngInject';
	
	$httpProvider.interceptors.push('TokenInterceptor');
}