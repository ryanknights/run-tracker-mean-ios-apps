export default function ($scope, $location, Auth, CurrentUser)
{	
	'ngInject';
	
	const vm = this;

	vm.currentUser = null;
	vm.isLoggedIn  = false;	
	vm.isAdmin     = false;

	vm.isActive = viewLocation => viewLocation === $location.path();

	vm.logOut = function ()
	{
		Auth.logOut();
		$location.path('/login');	
	}	

	$scope.$on('auth-login-success', (event) =>
	{
		vm.isLoggedIn  = true;
		vm.currentUser = CurrentUser.getUser();
		vm.isAdmin     = Auth.isAdmin();
	});

	$scope.$on('auth-login-reauthenticate-success', (event) =>
	{
		vm.isLoggedIn  = true;
		vm.currentUser = CurrentUser.getUser();
		vm.isAdmin     = Auth.isAdmin();
	});	

	$scope.$on('auth-login-failed', (event) =>
	{	
		vm.currentUser = null;
		vm.isLoggedIn  = false;
		vm.isAdmin     = false;
	});

	$scope.$on('auth-logout-success', (event) =>
	{
		vm.currentUser = null;
		vm.isLoggedIn  = false;
		vm.isAdmin     = false;
	});

	$scope.$on('auth-not-authenticated', (event) =>
	{
		vm.currentUser = null;
		vm.isLoggedIn  = false;
		vm.isAdmin     = false;
	});

	$scope.$on('auth-not-authorized', (event) =>
	{
		console.log('Event => auth-not-authorized');
	});	
}