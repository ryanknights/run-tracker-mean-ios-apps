export default function (run)
{	
	'ngInject';
	
	const vm = this;

	vm.run            = run;
	vm.distanceType   = 'miles';
	vm.paceType       = 'mpm';
	vm.run.date       = new Date(vm.run.date).getTime();

	console.log(vm.run.locations);
}