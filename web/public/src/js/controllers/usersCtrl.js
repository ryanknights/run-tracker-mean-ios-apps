export default function (users, Feedback, Users)
{	
	'ngInject';
	
	const vm = this;

	vm.users = users;

	vm.deleteUser = function (index, userID)
	{
		Users.deleteUser(userID).then(() =>
		{	
			Feedback.showMessage('success', 'User deleted');

			vm.users.splice(index, 1);

		}).catch((err) =>
		{	
			Feedback.showMessage('danger', err.data);			
		});
	}	
}