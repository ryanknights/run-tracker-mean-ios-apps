export default function (MyStats)
{	
	'ngInject';
	
	const vm = this;

	vm.totalDistanceType = 'miles';
	vm.paceType          = 'mpm';

	MyStats.totalDistance().then((res) =>
	{	
		vm.totalDistance = res.data.totalDistance;
	});

	MyStats.totalTime().then((res) =>
	{	
		vm.totalTime = res.data.totalTime;
	});	
}