export default function (userRuns, Feedback, Runs)
{	
	'ngInject';
	
	const vm = this;

	vm.runs         = userRuns;
	vm.distanceType = "miles";

	vm.runs.forEach((run) =>
	{
		run.date = new Date(run.date).getTime();
	});

	vm.deleteRun = function (index, runID)
	{
		Runs.deleteRun(runID).then(() =>
		{	
			Feedback.showMessage('success', 'Run deleted');

			vm.runs.splice(index, 1);

		}).catch((err) =>
		{	
			Feedback.showMessage('danger', err.data);			
		});
	}	
}