export default function ($location, Auth, Feedback)
{	
	'ngInject';
		
	var vm = this;

	vm.loading = false;

	vm.logIn = (username, password) =>
	{	
		vm.loading = true;

		Auth.logIn(username, password)
		
			.then(() =>
			{
				$location.path('/');
				
			}).catch(() =>
			{	
				Feedback.showMessage('danger', 'Login credentials incorrect, please try again.');

			}).finally(() =>
			{
				vm.loading = false;
			});
	}
}