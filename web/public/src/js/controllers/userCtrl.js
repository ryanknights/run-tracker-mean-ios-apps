export default function (user, Users, Feedback)
{	
	'ngInject';
		
	var vm = this;

	vm.user  = user;	

	vm.editUser = (userID, userData) =>
	{
		Users.updateUser(userID, userData).then((result) =>
		{
			if (result.data.success)
			{
				Feedback.showMessage('success', 'User is updated');
			}

		}).catch((err) =>
		{	
			Feedback.showMessage('warning', err.data);
		});
	}

	vm.updatePassword = (userID, newPassword) =>
	{
		Users.updatePassword(userID, newPassword).then((result) =>
		{
			if (result.data.success)
			{
				Feedback.showMessage('success', 'Password is updated');	
			}

		}).catch((err) =>
		{	
			Feedback.showMessage('warning', err.data);
		});
	}
}