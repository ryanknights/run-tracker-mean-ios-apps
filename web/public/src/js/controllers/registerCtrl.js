export default function ($location, Auth, Feedback)
{	
	'ngInject';
		
	var vm = this;

	vm.register = (username, email, password) =>
	{
		Auth.register(username, email, password).then(() =>
		{
			Auth.logIn(username, password)

				.then((result) =>
				{	
					$location.path('/');
					
				}).catch((err) =>
				{	
					Feedback.showMessage('warning', 'There was a problem logging you in.');
				});

		}).catch((err) =>
		{
			let errState = '';

			switch (err.status)
			{
				case 500: 
				{	
					errState = 'danger';
					break;
				}

				case 400:
				{
					errState = 'warning';
					break;
				}

				default:
				{
					errState = 'negative';
				}
			}			

			Feedback.showMessage(errState, err.data);
		});			
	}
}