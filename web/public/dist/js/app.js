(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _modulesConfig = require('./modules/config');

var _modulesConfig2 = _interopRequireDefault(_modulesConfig);

var _modulesControllers = require('./modules/controllers');

var _modulesControllers2 = _interopRequireDefault(_modulesControllers);

var _modulesRoutes = require('./modules/routes');

var _modulesRoutes2 = _interopRequireDefault(_modulesRoutes);

var _modulesServices = require('./modules/services');

var _modulesServices2 = _interopRequireDefault(_modulesServices);

var _modulesDirectives = require('./modules/directives');

var _modulesDirectives2 = _interopRequireDefault(_modulesDirectives);

var _modulesFilters = require('./modules/filters');

var _modulesFilters2 = _interopRequireDefault(_modulesFilters);

angular.module('run-tracker', ['ngRoute', 'chart.js', _modulesConfig2['default'].name, _modulesControllers2['default'].name, _modulesRoutes2['default'].name, _modulesServices2['default'].name, _modulesDirectives2['default'].name, _modulesFilters2['default'].name]);

},{"./modules/config":19,"./modules/controllers":20,"./modules/directives":21,"./modules/filters":22,"./modules/routes":23,"./modules/services":24}],2:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['$httpProvider', function ($httpProvider) {
	'ngInject';

	$httpProvider.interceptors.push('TokenInterceptor');
}];

module.exports = exports['default'];

},{}],3:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['$rootScope', '$location', '$window', 'Auth', function ($rootScope, $location, $window, Auth) {
			'ngInject';

			function preventNotAuthenticated(event) {
						event.preventDefault();

						$rootScope.$broadcast('auth-not-authenticated');

						$location.path('/login');

						Auth.userIsReauthenticating = false;
			}

			function preventNotAuthorized(event) {
						$rootScope.$broadcast('auth-not-authorized');

						$location.path('/');
			}

			if (window.sessionStorage.accessToken || window.sessionStorage.refreshToken) // If we have a token stored we assume a user has been logged in
						{
									Auth.userIsReauthenticating = true;
						}

			$rootScope.$on('$routeChangeStart', function (event, nextRoute, currentRoute) {
						if (nextRoute.access && nextRoute.access.requiredLogin && !Auth.isLoggedIn()) {
									if (Auth.userIsReauthenticating) {
												Auth.reAuthenticate().then(function (user) {
															if (!user) {
																		return preventNotAuthenticated(event);
															}
												});
									} else {
												return preventNotAuthenticated(event);
									}
						}
			});
}];

module.exports = exports['default'];

},{}],4:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['$scope', '$location', 'Auth', 'CurrentUser', function ($scope, $location, Auth, CurrentUser) {
	'ngInject';

	var vm = this;

	vm.currentUser = null;
	vm.isLoggedIn = false;
	vm.isAdmin = false;

	vm.isActive = function (viewLocation) {
		return viewLocation === $location.path();
	};

	vm.logOut = function () {
		Auth.logOut();
		$location.path('/login');
	};

	$scope.$on('auth-login-success', function (event) {
		vm.isLoggedIn = true;
		vm.currentUser = CurrentUser.getUser();
		vm.isAdmin = Auth.isAdmin();
	});

	$scope.$on('auth-login-reauthenticate-success', function (event) {
		vm.isLoggedIn = true;
		vm.currentUser = CurrentUser.getUser();
		vm.isAdmin = Auth.isAdmin();
	});

	$scope.$on('auth-login-failed', function (event) {
		vm.currentUser = null;
		vm.isLoggedIn = false;
		vm.isAdmin = false;
	});

	$scope.$on('auth-logout-success', function (event) {
		vm.currentUser = null;
		vm.isLoggedIn = false;
		vm.isAdmin = false;
	});

	$scope.$on('auth-not-authenticated', function (event) {
		vm.currentUser = null;
		vm.isLoggedIn = false;
		vm.isAdmin = false;
	});

	$scope.$on('auth-not-authorized', function (event) {
		console.log('Event => auth-not-authorized');
	});
}];

module.exports = exports['default'];

},{}],5:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['MyStats', function (MyStats) {
	'ngInject';

	var vm = this;

	vm.totalDistanceType = 'miles';
	vm.paceType = 'mpm';

	MyStats.totalDistance().then(function (res) {
		vm.totalDistance = res.data.totalDistance;
	});

	MyStats.totalTime().then(function (res) {
		vm.totalTime = res.data.totalTime;
	});
}];

module.exports = exports['default'];

},{}],6:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['$location', 'Auth', 'Feedback', function ($location, Auth, Feedback) {
	'ngInject';

	var vm = this;

	vm.loading = false;

	vm.logIn = function (username, password) {
		vm.loading = true;

		Auth.logIn(username, password).then(function () {
			$location.path('/');
		})['catch'](function () {
			Feedback.showMessage('danger', 'Login credentials incorrect, please try again.');
		})['finally'](function () {
			vm.loading = false;
		});
	};
}];

module.exports = exports['default'];

},{}],7:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['$location', 'Auth', 'Feedback', function ($location, Auth, Feedback) {
	'ngInject';

	var vm = this;

	vm.register = function (username, email, password) {
		Auth.register(username, email, password).then(function () {
			Auth.logIn(username, password).then(function (result) {
				$location.path('/');
			})['catch'](function (err) {
				Feedback.showMessage('warning', 'There was a problem logging you in.');
			});
		})['catch'](function (err) {
			var errState = '';

			switch (err.status) {
				case 500:
					{
						errState = 'danger';
						break;
					}

				case 400:
					{
						errState = 'warning';
						break;
					}

				default:
					{
						errState = 'negative';
					}
			}

			Feedback.showMessage(errState, err.data);
		});
	};
}];

module.exports = exports['default'];

},{}],8:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['run', function (run) {
	'ngInject';

	var vm = this;

	vm.run = run;
	vm.distanceType = 'miles';
	vm.paceType = 'mpm';
	vm.run.date = new Date(vm.run.date).getTime();

	console.log(vm.run.locations);
}];

module.exports = exports['default'];

},{}],9:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['runs', 'Feedback', 'Runs', function (runs, Feedback, Runs) {
	'ngInject';

	var vm = this;

	vm.runs = runs;
	vm.distanceType = "miles";

	vm.runs.forEach(function (run) {
		run.date = new Date(run.date).getTime();
	});

	vm.deleteRun = function (index, runID) {
		Runs.deleteRun(runID).then(function () {
			Feedback.showMessage('success', 'Run deleted');

			vm.runs.splice(index, 1);
		})['catch'](function (err) {
			Feedback.showMessage('danger', err.data);
		});
	};
}];

module.exports = exports['default'];

},{}],10:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['user', 'Users', 'Feedback', function (user, Users, Feedback) {
	'ngInject';

	var vm = this;

	vm.user = user;

	vm.editUser = function (userID, userData) {
		Users.updateUser(userID, userData).then(function (result) {
			if (result.data.success) {
				Feedback.showMessage('success', 'User is updated');
			}
		})['catch'](function (err) {
			Feedback.showMessage('warning', err.data);
		});
	};

	vm.updatePassword = function (userID, newPassword) {
		Users.updatePassword(userID, newPassword).then(function (result) {
			if (result.data.success) {
				Feedback.showMessage('success', 'Password is updated');
			}
		})['catch'](function (err) {
			Feedback.showMessage('warning', err.data);
		});
	};
}];

module.exports = exports['default'];

},{}],11:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['userRuns', 'Feedback', 'Runs', function (userRuns, Feedback, Runs) {
	'ngInject';

	var vm = this;

	vm.runs = userRuns;
	vm.distanceType = "miles";

	vm.runs.forEach(function (run) {
		run.date = new Date(run.date).getTime();
	});

	vm.deleteRun = function (index, runID) {
		Runs.deleteRun(runID).then(function () {
			Feedback.showMessage('success', 'Run deleted');

			vm.runs.splice(index, 1);
		})['catch'](function (err) {
			Feedback.showMessage('danger', err.data);
		});
	};
}];

module.exports = exports['default'];

},{}],12:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['users', 'Feedback', 'Users', function (users, Feedback, Users) {
	'ngInject';

	var vm = this;

	vm.users = users;

	vm.deleteUser = function (index, userID) {
		Users.deleteUser(userID).then(function () {
			Feedback.showMessage('success', 'User deleted');

			vm.users.splice(index, 1);
		})['catch'](function (err) {
			Feedback.showMessage('danger', err.data);
		});
	};
}];

module.exports = exports['default'];

},{}],13:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['allRuns', 'Feedback', 'Runs', function (allRuns, Feedback, Runs) {
	'ngInject';

	var vm = this;

	vm.runs = allRuns;
	vm.distanceType = "miles";

	vm.runs.forEach(function (run) {
		run.date = new Date(run.date).getTime();
	});

	vm.deleteRun = function (index, runID) {
		Runs.deleteRun(runID).then(function () {
			Feedback.showMessage('success', 'Run deleted');

			vm.runs.splice(index, 1);
		})['catch'](function (err) {
			Feedback.showMessage('danger', err.data);
		});
	};
}];

module.exports = exports['default'];

},{}],14:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = function () {
	'ngInject';

	return {

		restrict: 'AE',
		replace: true,
		templateUrl: '/src/js/views/directive-feedbackMessage.html',
		scope: {},
		controller: function controller(Feedback) {
			var vm = this;

			vm.message = Feedback.message;

			vm.closeMessage = function () {
				return Feedback.resetMessage();
			};
		},
		controllerAs: 'vm'
	};
};

module.exports = exports['default'];

},{}],15:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = function () {
	'ngInject';

	return {

		restrict: 'AE',
		replace: true,
		templateUrl: '/src/js/views/directive-runMap.html',
		scope: {
			locations: '='
		},
		bindToController: true,
		controller: function controller($scope, $element) {
			var vm = this;

			if (vm.locations === undefined) {
				vm.locations = [];
			}

			var map = undefined,
			    markers = [],
			    bounds = new google.maps.LatLngBounds(),
			    infoWindow = new google.maps.InfoWindow();

			function initMap() {
				map = new google.maps.Map($element[0]);

				if (vm.locations.length) {
					addMarker(vm.locations[0]);
					addMarker(vm.locations[vm.locations.length - 1]);
					addPolyline();
					fitBounds();
				}
			}

			function addPolyline() {
				var _loop = function (i, l) {
					if (i === 0) return 'continue';

					var beginLineCoords = { lat: vm.locations[i - 1].location.lat, lng: vm.locations[i - 1].location.lng },
					    endLineCoords = { lat: vm.locations[i].location.lat, lng: vm.locations[i].location.lng },
					    coords = [beginLineCoords, endLineCoords];

					var runPath = new google.maps.Polyline({
						path: coords,
						geodesic: true,
						strokeColor: '#' + (Math.random() * 0xFFFFFF << 0).toString(16),
						strokeOpacity: 1.0,
						strokeWeight: 2
					});

					google.maps.event.addListener(runPath, 'click', function () {
						infoWindow.setContent(vm.locations[i].description);
						infoWindow.open(map, marker);
					});

					runPath.setMap(map);
				};

				for (var i = 0, l = vm.locations.length; i < l; i++) {
					var _ret = _loop(i, l);

					if (_ret === 'continue') continue;
				}

				// vm.locations.forEach((data) =>
				// {	
				// 	coords.push({lat: data.location.lat, lng: data.location.lng})
				// });

				// var runPath = new google.maps.Polyline({
				// 	path          : coords,
				// 	geodesic      : true,
				// 	strokeColor   : '#FF0000',
				// 	strokeOpacity : 1.0,
				// 	strokeWeight  : 2
				// });

				// runPath.setMap(map);
			}

			function addMarker(data) {
				var marker = new google.maps.Marker({
					position: new google.maps.LatLng(data.location.lat, data.location.lng),
					map: map
				});

				google.maps.event.addListener(marker, 'click', function () {
					infoWindow.setContent(data.description);
					infoWindow.open(map, marker);
				});
			}

			function fitBounds() {
				var bounds = new google.maps.LatLngBounds();

				vm.locations.forEach(function (data) {
					var locationLatLng = new google.maps.LatLng(data.location.lat, data.location.lng);

					bounds.extend(locationLatLng);
				});

				map.fitBounds(bounds);
			}

			initMap();
		},
		controllerAs: 'vm'
	};
};

module.exports = exports['default'];

},{}],16:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = function () {
	'ngInject';

	return function (input, type) {
		console.log(input);

		if (!input) {
			return false;
		}

		var distance = undefined;

		switch (type) {
			case 'meters':
				distance = input;
				break;

			case 'km':
				distance = input / 1000;
				break;

			case 'miles':
				distance = input / 1609.344;
				break;
		}

		return distance.toFixed(2);
	};
};

module.exports = exports['default'];

},{}],17:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = function () {
	'ngInject';

	return function (seconds, meters, type) {
		if (!seconds) {
			return false;
		}

		var pace = undefined;

		switch (type) {
			case 'mpm':
				var minutes = seconds / 60,
				    miles = meters / 1609.344;

				pace = minutes / miles;
				break;

			case 'mph':
				var hours = seconds / 3600,
				    miles = meters / 1609.344;

				pace = miles / hours;
				break;

			case 'km/h':
				var hours = seconds / 3600,
				    km = meters / 1000;

				pace = km / hours;
				break;
		}

		return pace.toFixed(1);
	};
};

module.exports = exports['default'];

},{}],18:[function(require,module,exports){
"use strict";

exports.__esModule = true;

exports["default"] = function () {
	'ngInject';

	return function (input) {
		var sec_num = parseInt(input, 10),
		    hours = Math.floor(sec_num / 3600),
		    minutes = Math.floor((sec_num - hours * 3600) / 60),
		    seconds = sec_num - hours * 3600 - minutes * 60;

		if (hours < 10) {
			hours = "0" + hours;
		}
		if (minutes < 10) {
			minutes = "0" + minutes;
		}
		if (seconds < 10) {
			seconds = "0" + seconds;
		}

		return hours + 'h ' + minutes + 'm ' + seconds + 's';
	};
};

module.exports = exports["default"];

},{}],19:[function(require,module,exports){
'use strict';

exports.__esModule = true;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _configIncludeTokenInterceptor = require('../config/includeTokenInterceptor');

var _configIncludeTokenInterceptor2 = _interopRequireDefault(_configIncludeTokenInterceptor);

var _configSecurity = require('../config/security');

var _configSecurity2 = _interopRequireDefault(_configSecurity);

var _module = angular.module('run-tracker.config', []).config(_configIncludeTokenInterceptor2['default']).run(_configSecurity2['default']);

exports['default'] = _module;
module.exports = exports['default'];

},{"../config/includeTokenInterceptor":2,"../config/security":3}],20:[function(require,module,exports){
'use strict';

exports.__esModule = true;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _controllersApplicationCtrl = require('../controllers/applicationCtrl');

var _controllersApplicationCtrl2 = _interopRequireDefault(_controllersApplicationCtrl);

var _controllersHomeCtrl = require('../controllers/homeCtrl');

var _controllersHomeCtrl2 = _interopRequireDefault(_controllersHomeCtrl);

var _controllersRunsCtrl = require('../controllers/runsCtrl');

var _controllersRunsCtrl2 = _interopRequireDefault(_controllersRunsCtrl);

var _controllersRunCtrl = require('../controllers/runCtrl');

var _controllersRunCtrl2 = _interopRequireDefault(_controllersRunCtrl);

var _controllersLoginCtrl = require('../controllers/loginCtrl');

var _controllersLoginCtrl2 = _interopRequireDefault(_controllersLoginCtrl);

var _controllersRegisterCtrl = require('../controllers/registerCtrl');

var _controllersRegisterCtrl2 = _interopRequireDefault(_controllersRegisterCtrl);

var _controllersUsersCtrl = require('../controllers/usersCtrl');

var _controllersUsersCtrl2 = _interopRequireDefault(_controllersUsersCtrl);

var _controllersUserCtrl = require('../controllers/userCtrl');

var _controllersUserCtrl2 = _interopRequireDefault(_controllersUserCtrl);

var _controllersUsersRunsCtrl = require('../controllers/usersRunsCtrl');

var _controllersUsersRunsCtrl2 = _interopRequireDefault(_controllersUsersRunsCtrl);

var _controllersUserRunsCtrl = require('../controllers/userRunsCtrl');

var _controllersUserRunsCtrl2 = _interopRequireDefault(_controllersUserRunsCtrl);

var _module = angular.module('run-tracker.controllers', []).controller('applicationCtrl', _controllersApplicationCtrl2['default']).controller('homeCtrl', _controllersHomeCtrl2['default']).controller('runsCtrl', _controllersRunsCtrl2['default']).controller('runCtrl', _controllersRunCtrl2['default']).controller('loginCtrl', _controllersLoginCtrl2['default']).controller('registerCtrl', _controllersRegisterCtrl2['default']).controller('usersCtrl', _controllersUsersCtrl2['default']).controller('userCtrl', _controllersUserCtrl2['default']).controller('usersRunsCtrl', _controllersUsersRunsCtrl2['default']).controller('userRunsCtrl', _controllersUserRunsCtrl2['default']);

exports['default'] = _module;
module.exports = exports['default'];

},{"../controllers/applicationCtrl":4,"../controllers/homeCtrl":5,"../controllers/loginCtrl":6,"../controllers/registerCtrl":7,"../controllers/runCtrl":8,"../controllers/runsCtrl":9,"../controllers/userCtrl":10,"../controllers/userRunsCtrl":11,"../controllers/usersCtrl":12,"../controllers/usersRunsCtrl":13}],21:[function(require,module,exports){
'use strict';

exports.__esModule = true;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _directivesFeedbackMessage = require('../directives/feedbackMessage');

var _directivesFeedbackMessage2 = _interopRequireDefault(_directivesFeedbackMessage);

var _directivesRunMap = require('../directives/runMap');

var _directivesRunMap2 = _interopRequireDefault(_directivesRunMap);

var _module = angular.module('run-tracker.directives', []).directive('feedbackMessage', _directivesFeedbackMessage2['default']).directive('runMap', _directivesRunMap2['default']);

exports['default'] = _module;
module.exports = exports['default'];

},{"../directives/feedbackMessage":14,"../directives/runMap":15}],22:[function(require,module,exports){
'use strict';

exports.__esModule = true;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _filtersDistanceType = require('../filters/distanceType');

var _filtersDistanceType2 = _interopRequireDefault(_filtersDistanceType);

var _filtersSecondsToTime = require('../filters/secondsToTime');

var _filtersSecondsToTime2 = _interopRequireDefault(_filtersSecondsToTime);

var _filtersPaceType = require('../filters/paceType');

var _filtersPaceType2 = _interopRequireDefault(_filtersPaceType);

var _module = angular.module('run-tracker.filters', []).filter('distanceType', _filtersDistanceType2['default']).filter('secondsToTime', _filtersSecondsToTime2['default']).filter('paceType', _filtersPaceType2['default']);

exports['default'] = _module;
module.exports = exports['default'];

},{"../filters/distanceType":16,"../filters/paceType":17,"../filters/secondsToTime":18}],23:[function(require,module,exports){
'use strict';

exports.__esModule = true;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _routesMain = require('../routes/main');

var _routesMain2 = _interopRequireDefault(_routesMain);

var _module = angular.module('runTracker.routes', []).config(_routesMain2['default']);

exports['default'] = _module;
module.exports = exports['default'];

},{"../routes/main":25}],24:[function(require,module,exports){
'use strict';

exports.__esModule = true;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _servicesAuthService = require('../services/AuthService');

var _servicesAuthService2 = _interopRequireDefault(_servicesAuthService);

var _servicesCurrentUserService = require('../services/CurrentUserService');

var _servicesCurrentUserService2 = _interopRequireDefault(_servicesCurrentUserService);

var _servicesTokenInterceptor = require('../services/TokenInterceptor');

var _servicesTokenInterceptor2 = _interopRequireDefault(_servicesTokenInterceptor);

var _servicesFeedback = require('../services/Feedback');

var _servicesFeedback2 = _interopRequireDefault(_servicesFeedback);

var _servicesRuns = require('../services/Runs');

var _servicesRuns2 = _interopRequireDefault(_servicesRuns);

var _servicesUsers = require('../services/Users');

var _servicesUsers2 = _interopRequireDefault(_servicesUsers);

var _servicesMyStats = require('../services/MyStats');

var _servicesMyStats2 = _interopRequireDefault(_servicesMyStats);

var _module = angular.module('gps-tracker.services', []).factory('Auth', _servicesAuthService2['default']).factory('CurrentUser', _servicesCurrentUserService2['default']).factory('TokenInterceptor', _servicesTokenInterceptor2['default']).factory('Feedback', _servicesFeedback2['default']).factory('Runs', _servicesRuns2['default']).factory('Users', _servicesUsers2['default']).factory('MyStats', _servicesMyStats2['default']);

exports['default'] = _module;
module.exports = exports['default'];

},{"../services/AuthService":27,"../services/CurrentUserService":28,"../services/Feedback":29,"../services/MyStats":30,"../services/Runs":31,"../services/TokenInterceptor":32,"../services/Users":33}],25:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
	'ngInject';

	var _resolves = require('./resolves');

	$routeProvider.when('/', {
		templateUrl: '/src/js/views/home.html',
		controller: 'homeCtrl',
		controllerAs: 'home',
		access: { requiredLogin: true }
	}).when('/runs', {
		templateUrl: '/src/js/views/runs.html',
		controller: 'runsCtrl',
		controllerAs: 'runs',
		access: { requiredLogin: true },
		resolve: { runs: _resolves.runs }
	}).when('/runs/:id', {
		templateUrl: '/src/js/views/run.html',
		controller: 'runCtrl',
		controllerAs: 'run',
		access: { requiredLogin: true },
		resolve: { run: _resolves.run }
	}).when('/users-runs', {
		templateUrl: '/src/js/views/users-runs.html',
		controller: 'usersRunsCtrl',
		controllerAs: 'usersRuns',
		access: { requiredLogin: true, isAdmin: true },
		resolve: { allRuns: _resolves.allRuns }
	}).when('/users-runs/:id', {
		templateUrl: '/src/js/views/user-runs.html',
		controller: 'userRunsCtrl',
		controllerAs: 'userRuns',
		access: { requiredLogin: true, isAdmin: true },
		resolve: { userRuns: _resolves.userRuns }
	}).when('/users', {
		templateUrl: '/src/js/views/users.html',
		controller: 'usersCtrl',
		controllerAs: 'users',
		access: { requiredLogin: true },
		resolve: { users: _resolves.users }
	}).when('/users/:id', {
		templateUrl: 'src/js/views/user.html',
		controller: 'userCtrl',
		controllerAs: 'user',
		access: { requiredLogin: true },
		resolve: { user: _resolves.user }
	}).when('/login', {
		templateUrl: '/src/js/views/login.html',
		controller: 'loginCtrl',
		controllerAs: 'login',
		access: { requiredLogin: false }
	}).when('/register', {
		templateUrl: '/src/js/views/register.html',
		controller: 'registerCtrl',
		controllerAs: 'register',
		access: { requiredLogin: false }
	});
}];

module.exports = exports['default'];

},{"./resolves":26}],26:[function(require,module,exports){
/*----------  Resolve Admin Users  ----------*/

"use strict";

exports.__esModule = true;
exports.users = users;
exports.user = user;
exports.runs = runs;
exports.run = run;
exports.allRuns = allRuns;
exports.userRuns = userRuns;

function users($q, Users) {
	return Users.retrieveUsers().then(function (res) {
		return res.data.users;
	}, function (res) {
		return $q.reject(res);
	});
}

/*----------  Resolve Admin User  ----------*/

function user($q, $route, Users) {
	return Users.retrieveUser($route.current.params.id).then(function (res) {
		return res.data.user;
	}, function (res) {
		return $q.reject(res);
	});
}

/*----------  Resolve Runs  ----------*/

function runs($q, Runs) {
	return Runs.retrieveRuns().then(function (res) {
		return res.data.runs;
	}, function (res) {
		return $q.reject(res);
	});
}

/*----------  Resolve Run  ----------*/

function run($q, $route, Runs) {
	return Runs.retrieveRun($route.current.params.id).then(function (res) {
		return res.data.run;
	}, function (res) {
		return $q.reject(res);
	});
}

/*----------  All Runs  ----------*/

function allRuns($q, Runs) {
	return Runs.retrieveAllRuns().then(function (res) {
		return res.data.runs;
	}, function (res) {
		return $q.reject(res);
	});
}

/*----------  User Runs  ----------*/

function userRuns($q, $route, Runs) {
	return Runs.retrieveUserRuns($route.current.params.id).then(function (res) {
		return res.data.runs;
	}, function (res) {
		return $q.reject(res);
	});
}

},{}],27:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['$rootScope', '$http', 'CurrentUser', '$q', function ($rootScope, $http, CurrentUser, $q) {
	'ngInject';

	var authService = {};

	authService.userIsReauthenticating = false;

	authService.isLoggedIn = function () {
		return CurrentUser.user !== null;
	};

	authService.isAdmin = function () {
		if (!CurrentUser.user) {
			return false;
		}

		return CurrentUser.user.isAdmin === true;
	};

	authService.register = function (username, email, password) {
		return $http.post('/api/register', { username: username, email: email, password: password });
	};

	authService.logIn = function (username, password) {
		var deferred = $q.defer();

		$http.post('/api/login', { username: username, password: password }).then(function (res) {
			CurrentUser.setUser(res.data.user, res.data.token);

			$rootScope.$broadcast('auth-login-success');

			deferred.resolve();
		}, function () {
			$rootScope.$broadcast('auth-login-failed');

			deferred.reject();
		});

		return deferred.promise;
	};

	authService.logOut = function () {
		authService.userIsReauthenticating = false;

		CurrentUser.destroy();

		$rootScope.$broadcast('auth-logout-success');
	};

	authService.reAuthenticate = function () {
		return $http.get('/api/authenticate').then(function (res) {
			CurrentUser.setUser(res.data.user);

			authService.userIsReauthenticating = false;

			$rootScope.$broadcast('auth-login-reauthenticate-success');

			return res.data.user;
		}, function () {
			$rootScope.$broadcast('auth-login-reauthenticate-failed');

			return null;
		});
	};

	return authService;
}];

module.exports = exports['default'];

},{}],28:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['$window', function ($window) {
	'ngInject';

	var CurrentUserService = {};

	CurrentUserService.user = null;

	CurrentUserService.setUser = function (user, tokens) {
		if (user === undefined) {
			throw new Error('setUser requires a valid user object');
			return false;
		}

		CurrentUserService.user = {};

		CurrentUserService.user.userid = user.userid;
		CurrentUserService.user.username = user.username;
		CurrentUserService.user.isAdmin = user.isAdmin;

		if (tokens !== undefined) {
			$window.sessionStorage.accessToken = tokens.access;
			$window.sessionStorage.refreshToken = tokens.refresh;
		}
	};

	CurrentUserService.getUser = function () {
		return CurrentUserService.user !== null ? CurrentUserService.user : false;
	};

	CurrentUserService.destroy = function () {
		CurrentUserService.user = null;

		delete $window.sessionStorage.accessToken;
		delete $window.sessionStorage.refreshToken;
	};

	return CurrentUserService;
}];

module.exports = exports['default'];

},{}],29:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['$rootScope', function ($rootScope) {
	'ngInject';

	var feedbackService = {};

	$rootScope.$on('$routeChangeSuccess', function () {
		feedbackService.resetMessage();
	});

	feedbackService.message = { text: null, state: null };

	feedbackService.resetMessage = function () {
		feedbackService.message.state = null;
		feedbackService.message.text = null;
	};

	feedbackService.showMessage = function (state, text) {
		if (state === undefined) {
			throw new Error('Please define a state for a feedback message');
		}

		if (text === undefined) {
			throw new Error('Please enter the message for a feedback alert');
		}

		feedbackService.message.state = state;
		feedbackService.message.text = text;
	};

	return feedbackService;
}];

module.exports = exports['default'];

},{}],30:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['$http', function ($http) {
	'ngInject';

	var MyStats = {};

	MyStats.totalDistance = function () {
		return $http.get('/api/mystats/totaldistance');
	};
	MyStats.totalTime = function () {
		return $http.get('/api/mystats/totaltime');
	};

	return MyStats;
}];

module.exports = exports['default'];

},{}],31:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['$http', function ($http) {
	'ngInject';

	var Runs = {};

	Runs.retrieveRuns = function () {
		return $http.get('/api/runs');
	};
	Runs.deleteRun = function (runID) {
		return $http['delete']('/api/runs/' + runID);
	};
	Runs.retrieveRun = function (runID) {
		return $http.get('/api/runs/' + runID);
	};
	Runs.retrieveAllRuns = function () {
		return $http.get('/api/runs/all');
	};
	Runs.retrieveUserRuns = function (runID) {
		return $http.get('/api/runs/all/' + runID);
	};

	return Runs;
}];

module.exports = exports['default'];

},{}],32:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['$q', '$rootScope', '$window', '$location', '$timeout', '$injector', 'CurrentUser', function ($q, $rootScope, $window, $location, $timeout, $injector, CurrentUser) {
	'ngInject';

	var tokenInterceptor = {};

	var $http = undefined;

	$timeout(function () {
		$http = $injector.get('$http');
	});

	tokenInterceptor.request = function (config) {
		config.headers = config.headers || {};

		if ($window.sessionStorage.accessToken) {
			config.headers.Authorization = 'Bearer ' + $window.sessionStorage.accessToken;
		}

		return config;
	};

	tokenInterceptor.responseError = function (rejection) {
		if (rejection != null && rejection.status === 401 && ($window.sessionStorage.accessToken || CurrentUser.user)) {
			CurrentUser.destroy();
			$location.path('/login');
			$rootScope.$broadcast('auth-not-authenticated');
		}

		if (rejection != null && rejection.status === 403 && ($window.sessionStorage.accessToken || CurrentUser.user)) {
			$location.path('/');
			$rootScope.$broadcast('auth-not-authorized');
		}

		if (rejection != null && rejection.status === 500) {
			console.log('tokenInterceptor.js - 500 error handling');
		}

		return $q.reject(rejection);
	};

	return tokenInterceptor;
}];

module.exports = exports['default'];

},{}],33:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['$http', function ($http) {
	'ngInject';

	var Users = {};

	Users.retrieveUsers = function () {
		return $http.get('/api/users');
	};
	Users.deleteUser = function (userID) {
		return $http['delete']('/api/users/' + userID);
	};
	Users.retrieveUser = function (userID) {
		return $http.get('/api/users/' + userID);
	};
	Users.updateUser = function (userID, data) {
		return $http.put('/api/users/' + userID, data);
	};
	Users.updatePassword = function (userID, newPassword) {
		return $http.put('/api/users/' + userID + '/password', { newPassword: newPassword });
	};

	return Users;
}];

module.exports = exports['default'];

},{}]},{},[1]);
