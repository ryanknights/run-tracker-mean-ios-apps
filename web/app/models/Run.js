var mongoose = require('mongoose');

var runSchema = mongoose.Schema(
{	
	userid    : String,
	date      : String,
	meters    : Number,
	seconds   : Number,
	locations :
	{
		type    : Array,
		default : []
	}
});

module.exports = mongoose.model('Run', runSchema);

