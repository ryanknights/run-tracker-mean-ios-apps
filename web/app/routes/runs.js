"use strict"

const express   = require('express'),
	  User      = require('../models/User'),
	  runsCtrl  = require('../controllers/runs');

function checkForUser (req, res, next)
{
	if (!req.user)
	{
		return res.send(401);
	}

	return next();
}

function checkForAdmin (req, res, next)
{
	User.findOne({_id: req.user.userid}).exec().then((user) =>
	{
		if (user.isAdmin !== true)
		{
			return res.send(403);
		}

		return next();

	}).catch((err) =>
	{
		return res.send(403);
	});
}

module.exports = (() =>
{
	var api = express.Router();

	api.use(checkForUser);

	api.post('/', runsCtrl.add);
	api.get('/', runsCtrl.all);
	api.get('/all', checkForAdmin, runsCtrl.adminAll);
	api.get('/all/:id', checkForAdmin, runsCtrl.adminUserRuns);
	api.get('/:id', runsCtrl.single);
	api.delete('/:id', runsCtrl.delete);
	
	return api;

})();