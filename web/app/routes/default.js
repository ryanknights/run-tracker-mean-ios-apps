"use strict"

const express     = require('express'),
	  defaultCtrl = require('../controllers/default');

module.exports = (() =>
{
	var api = express.Router();

	api.get('/*', defaultCtrl.sendIndex);

	return api;

})();