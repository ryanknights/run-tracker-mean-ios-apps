"use strict"

const express      = require('express'),
	  mystatsCtrl  = require('../controllers/mystats');

function checkForUser (req, res, next)
{
	if (!req.user)
	{
		return res.send(401);
	}

	return next();
}

module.exports = (() =>
{
	var api = express.Router();

	api.use(checkForUser);

	api.get('/totaldistance', mystatsCtrl.totalDistance);
	api.get('/totaltime', mystatsCtrl.totalTime);

	return api;

})();