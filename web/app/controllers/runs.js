"use strict";

const Run = require('../models/Run');

exports.add = (req, res, next) =>
{
	const userID = req.user.userid;
	
	const newRun = new Run(
	{
		userid : userID,
		date   : req.body.date,
		meters : req.body.meters,
		seconds : req.body.seconds,
		locations : req.body.locations
	});

	newRun.save().then((run) =>
	{	
		if (!run)
		{
			return res.send(404);
		}

		return res.json({success: true, run: run});

	}).catch((err) =>
	{
		return res.send(500)
	});
}

exports.all = (req, res, next) =>
{
	const userID = req.user.userid;

	Run.find({userid: userID}).exec().then((runs) =>
	{
		if (!runs)
		{
			return res.send(404);
		}

		return res.json({success: true, runs: runs});

	}).catch((err) =>
	{
		return res.send(500);
	});
}

exports.single = (req, res, next) =>
{
	const userID = req.user.userid,
		  runID  = req.params.id;

	Run.findOne({_id: runID}).exec().then((run) =>
	{
		if (!run)
		{
			return res.send(404);
		}
		
		if (run.userid !== userID && req.user.isAdmin !== true)
		{
			return res.send(403);
		}

		return res.json({success: true, run: run});

	}).catch((err) =>
	{	
		return res.send(500);
	});
}

exports.delete = (req, res, next) =>
{
	const userID = req.user.userid,
		  runID  = req.params.id;

	Run.findOne({_id: runID}).exec().then((run) =>
	{
		if (!run)
		{
			return res.send(404);
		}

		if (run.userid !== userID)
		{
			return res.send(403);
		}

		return Run.remove({_id : runID}).exec();

	}).then((run) =>
	{
		return res.json({success: true, run: run});

	}).catch((err) =>
	{	
		return res.send(500);
	});
}

exports.adminAll = (req, res, next) =>
{
	Run.find().exec().then((runs) =>
	{
		if (!runs)
		{
			return res.send(404);
		}

		return res.json({success: true, runs: runs});

	}).catch((err) =>
	{
		return res.send(500);
	});	
}

exports.adminUserRuns = (req, res, next) =>
{
	Run.find({userid: req.params.id}).exec().then((runs) =>
	{
		return res.json({success: true, runs: runs});

	}).catch((err) =>
	{
		return res.send(500);
	});
}