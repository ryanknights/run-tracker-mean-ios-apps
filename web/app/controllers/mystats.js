"use strict";

const Run  = require('../models/Run'),
	  User = require('../models/User');

exports.totalDistance = (req, res, next) =>
{
	const userID = req.user.userid;

	Run.aggregate(
    [ 
    	{ $match : { userid : userID } },
    	{ $group : { _id: null, totalDistance : { $sum : '$meters'} } }

    ], (err, result) =>
    {
	    if (err) 
	    {
	        return res.send(500, 'There was a problem returning the total distance');
	    }

	    const totalDistance = (result[0] !== undefined)? result[0].totalDistance : 0;

	    return res.json({success: true, totalDistance: totalDistance});	
    });
}

exports.totalTime = (req, res, next) =>
{
	const userID = req.user.userid;

	Run.aggregate(
    [ 
    	{ $match : { userid : userID } },
    	{ $group : { _id: null, totalTime : { $sum : '$seconds'} } }

    ], (err, result) =>
    {
	    if (err) 
	    {
	        return res.send(500, 'There was a problem returning the total time');
	    }

	    const totalTime = (result[0] !== undefined)? result[0].totalTime : 0;

	    return res.json({success: true, totalTime: totalTime});	
    });

}