import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/subscription';

import { LoginPage } from '../login-page/login-page';
import { RegisterPage } from '../register-page/register-page';
import { HomePage } from '../home/home';
import { AuthService } from '../../providers/auth-service';


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage implements OnDestroy {

  tab1Root = LoginPage;
  tab2Root = RegisterPage;
  tab3Root = HomePage;

  userLoggedInSubscription: Subscription;
  userLoggedIn: boolean;

  constructor(public auth: AuthService) {
  	this.userLoggedInSubscription = this.auth.getUserStatus().subscribe((isLoggedIn) =>
  	{	
      console.log(isLoggedIn);
  		this.userLoggedIn = isLoggedIn;
  	});
  }

  logout () {
  	this.auth.logout().subscribe((success) => {});
  }

  ngOnDestroy() {
  	this.userLoggedInSubscription.unsubscribe();
  }
}
