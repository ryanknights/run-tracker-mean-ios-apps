import { Component, NgZone } from '@angular/core';
import { Platform } from 'ionic-angular';
import { NavController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { Subscription } from 'rxjs';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/filter';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  locations: any;
  watch: any;
  isTracking: Boolean;
  onResumeSubscription: Subscription;
  onPauseSubscription: Subscription;
  tokens: any;
  user: any;

  constructor(public navCtrl: NavController, 
  			  public zone: NgZone, 
  			  public backgroundGeolocation: BackgroundGeolocation, 
  			  public geolocation: Geolocation,
          public platform: Platform,
          public storage: Storage,
          public auth: AuthService
  ) 
  {
  	this.locations = [];

    let config = {
      desiredAccuracy: 0,
      stationaryRadius: 20,
      distanceFilter: 10,
      debug: true,
      interval: 2000
    };

    this.backgroundGeolocation.configure(config).subscribe((location) =>
    {
      console.log(`BackgroundGeolocation: ${location.latitude}, ${location.longitude}`);

      this.zone.run(() =>
      {
        this.locations.push(
        {
          lat: location.latitude,
          lng: location.longitude
        });
      });

      this.backgroundGeolocation.finish();

    }, (err) =>
    {
      console.log(err);
    });

    this.onResumeSubscription = platform.resume.subscribe(() =>
    {
      this.locations.push(
      {
        message: 'Background geolocation stopped'
      });

      this.stopBackgroundGeolocation();
    });    

    this.onPauseSubscription = platform.pause.subscribe(() =>
    {
      this.locations.push(
      {
        message: 'Background geolocation started'
      });

      this.startBackgroundGeolocation();
    });

    storage.get('tokens').then((val) =>
    {
      this.tokens = val;
    });

    this.user = this.auth.getUserInfo();
  }

  startGeolocation () {

      this.locations.push(
      {
        message: 'Geolocation started'
      });

    let options = {
      frequency: 3000, 
      enableHighAccuracy: true
    };

    this.watch = this.geolocation.watchPosition(options)
            .filter((p: any) => p.code === undefined)
            .subscribe((position: Geoposition) =>
            {
              console.log(position);

              this.zone.run(() =>
              {
                this.locations.push(
                {
                  lat: position.coords.latitude,
                  lng: position.coords.longitude
                });                
              });
            });
  }

  startBackgroundGeolocation () {
    this.backgroundGeolocation.start();
  }

  stopGeolocation () {
      this.locations.push(
      {
        message: 'Geolocation stopped'
      });    
    this.watch.unsubscribe();
  }

  stopBackgroundGeolocation () {
    this.backgroundGeolocation.finish();
  }
  
  startTracking () {
    if (this.isTracking)
    {
      return;
    }

    this.isTracking = true;
    this.startGeolocation();
  }

  stopTracking () {
    if (!this.isTracking)
    {
      return;
    }
    
    this.isTracking = false;
    this.stopGeolocation();

  }

}
