import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';

/**
 * Generated class for the RegisterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-register-page',
  templateUrl: 'register-page.html',
})
export class RegisterPage {

	loading: Loading;
	credentials = { username: '', password: '', email: ''};

  constructor(public navCtrl: NavController, public navParams: NavParams, public auth: AuthService, public alertCtrl: AlertController, public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {

  }

  register () {
  	this.showLoading();
  	this.auth.register(this.credentials)
      .subscribe((success) =>
    	{
        if (success) {
					this.auth.login(this.credentials)
					  .subscribe((success) =>
						{
					    this.loading.dismiss();
							
						}, (error) =>
						{
							this.showError(error._body);
						});        	
        } else {
        	this.showError('There was a problem registering');
        }
    	}, (error) =>
    	{
    		this.showError(error._body);
    	});
  }

  showLoading () {
  	this.loading = this.loadingCtrl.create({
  		content: 'Please wait...',
  		dismissOnPageChange: true
  	});
  	this.loading.present();
  }

  showError(text) {
  	this.loading.dismiss();

  	let alert = this.alertCtrl.create({
  		title: 'Fail',
  		subTitle: text,
  		buttons: ['OK']
  	});
  	alert.present(prompt);
  }  

}
