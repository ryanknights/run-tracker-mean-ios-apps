import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login-page',
  templateUrl: 'login-page.html',
})
export class LoginPage {

	loading: Loading;
	credentials = { username: '', password: ''};

  constructor(public navCtrl: NavController, public navParams: NavParams, public auth: AuthService, public alertCtrl: AlertController, public loadingCtrl: LoadingController, public storage: Storage) {
    storage.get('tokens').then((val) =>
    {
      if (val) {
        this.showLoading();
        this.auth.authenticate()
          .subscribe((success) =>
          {
            this.loading.dismiss();
          }, (error) => {
            this.showError(error._body);
          });
      }
    });    
  }

  ionViewDidLoad() {

  }

  login () {
  	this.showLoading();
  	this.auth.login(this.credentials)
      .subscribe((success) =>
    	{
        this.loading.dismiss();
    		
    	}, (error) =>
    	{
    		this.showError(error.statusText);
    	});
  }

  showLoading () {
  	this.loading = this.loadingCtrl.create({
  		content: 'Please wait...',
  		dismissOnPageChange: true
  	});
  	this.loading.present();
  }

  showError(text) {
  	this.loading.dismiss();

  	let alert = this.alertCtrl.create({
  		title: 'Fail',
  		subTitle: text,
  		buttons: ['OK']
  	});
  	alert.present(prompt);
  }

}
