import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Headers } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';

/*
  Generated class for the AuthService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/

export class User {
	username: string;
	id: string;
	isAdmin: boolean;

	constructor (username: string, id: string, isAdmin: boolean) {
		this.username = username;
		this.id = id;
		this.isAdmin = isAdmin;
	}
}

@Injectable()
export class AuthService {

	currentUser: User;
	api: string = 'http://runtracker.ryanknights.co.uk/api';
	userLoggedInSubject = new Subject<any>();

	constructor (public http: Http, public storage: Storage) {

	}

 	login (credentials) {
 		if (credentials['username'] === null || credentials['password'] === null) {
 			return Observable.throw('Please insert credentials');
 		}
 		return Observable.create(observer => {

 			this.http.post(`${this.api}/login`, {username: credentials['username'], password: credentials['password']})
 				.map(res => res.json())
 				.subscribe((userData) =>
 				{
 					this.currentUser = new User(userData.user.username, userData.user.userid, userData.user.isAdmin);
 					this.userLoggedInSubject.next(true);
 					this.setTokens(userData.token.access, userData.token.refresh);
		 			observer.next(true);
		 			observer.complete(); 			

 				}, (error) =>
 				{
						observer.error(error);
						observer.complete(); 
 				});
 		});
 	}

 	authenticate () {
 		return Observable.create(observer => {
	 		this.http.get(`${this.api}/authenticate`)
 					.map(res => res.json())
 					.subscribe((userData) => {
	 					this.currentUser = new User(userData.user.username, userData.user.userid, userData.user.isAdmin);
	 					this.userLoggedInSubject.next(true);
			 			observer.next(true);
			 			observer.complete(); 	 						
 					}, (error) => {
						observer.error(error);
						observer.complete();  						
 					});
 		});
 	}

 	register (credentials) {
 		if (credentials['username'] === null || credentials['email'] === null || credentials['password'] === null) {
 			return Observable.throw('Please insert credentials');
 		}

 		return Observable.create(observer => {
 			this.http.post(`${this.api}/register`, {username: credentials['username'], email: credentials['email'], password: credentials['password']})
 				.map(res => res.json())
 				.subscribe((success) => {
 					observer.next(true);
 					observer.complete();
 				}, (error) => {
 					observer.error(error);
 					observer.complete();
 				})
 		});
 	}

 	logout () {
 		return Observable.create(observer => {
 			this.currentUser = null;
 			this.userLoggedInSubject.next(false);
 			this.removeTokens();
 			observer.next();
 			observer.complete();
 		});
 	}

 	refreshToken (refreshToken) {
 		return Observable.create(observer => {
	 		this.http.post(`${this.api}/refreshToken`, {token: refreshToken})
 					.map(res => res.json())
 					.subscribe((data) => {
	 					this.setTokens(data.token.access, data.token.refresh);
			 			observer.next(true);
			 			observer.complete(); 	 						
 					}, (error) => {
						observer.error(error);
						observer.complete();  						
 					});
 		}); 		
 	}

 	getUserInfo(): User {
 		return this.currentUser;
 	}

 	getUserStatus (): Observable<any> {
 		return this.userLoggedInSubject.asObservable();
 	}

 	setTokens (access, refresh) {
 		this.storage.set('tokens', {access: access, refresh: refresh});
 	}

 	removeTokens() {
 		this.storage.remove('tokens');
 	}

}
