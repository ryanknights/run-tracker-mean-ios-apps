import { Http, Request, RequestOptions, RequestOptionsArgs, Response, XHRBackend, Headers } from "@angular/http";
import { Injectable, Injector } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { Storage } from '@ionic/storage';
import "rxjs/add/observable/fromPromise";
import "rxjs/add/operator/mergeMap";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/finally';
import { AuthService } from './auth-service';

@Injectable()
export class HttpService extends Http {

private origRequest: Request;
private auth: AuthService;

  constructor(backend: XHRBackend, defaultOptions: RequestOptions, public storage: Storage, public injector: Injector) {
    super(backend, defaultOptions);
    setTimeout(() => this.auth = injector.get(AuthService));
  }

  private getRequestOptionArgs(options?: RequestOptionsArgs) : RequestOptionsArgs {
      if (options == null) {
          options = new RequestOptions();
      }
      if (options.headers == null) {
          options.headers = new Headers();
      }
      options.headers.append('Content-Type', 'application/json');
      return options;
  }

  protected requestWithToken(req: Request, tokens: any): Observable<Response> {
    this.origRequest = req;
    if (tokens) {
      req.headers.set('Authorization', 'Bearer ' + tokens.access);
    }

    return super.request(req);
  }

  request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
    if (typeof url === 'string') {
      return this.get(url, options);
    }
    let req: Request = url as Request;
    let tokens: Promise<string> = this.getToken();
    return Observable.fromPromise(tokens).mergeMap((tokens: any) => this.requestWithToken(req, tokens));
  }

  get(url: string, options?: RequestOptionsArgs, noIntercept?: boolean): Observable<Response> {
    if (noIntercept) {
      return super.get(url, options)
              .do(this.successRequest)
              .finally(this.afterRequest);        
    }
    return this.intercept(super.get(url, options));
  }

  post(url: string, body: any, options?: RequestOptionsArgs, noIntercept?: boolean): Observable<Response> {
    if (noIntercept) {
      return super.post(url, body, options)
              .do(this.successRequest)
              .finally(this.afterRequest);      
    }
    return this.intercept(super.post(url, body, this.getRequestOptionArgs(options)));
  }

  put(url: string, body: any, options?: RequestOptionsArgs, noIntercept?: boolean): Observable<Response> {
    if (noIntercept) {
      return super.put(url, body, options)
              .do(this.successRequest)
              .finally(this.afterRequest);     
    }
    return this.intercept(super.put(url, body, this.getRequestOptionArgs(options)));
  }

  delete(url: string, options?: RequestOptionsArgs, noIntercept?: boolean): Observable<Response> {
    if (noIntercept) {
      return super.delete(url, options)
              .do(this.successRequest)
              .finally(this.afterRequest);      
    }
    return this.intercept(super.delete(url, options));
  }

  protected intercept(observable: Observable<Response>): Observable<Response> {
    return observable
      .do(this.successRequest)
      .finally(this.afterRequest)
      .catch(this.errorRequest.bind(this));
      
  }

  private successRequest (response) {

  }

  private errorRequest (err, source) {

    switch (err.status)
    {
      case 401:
        if (err._body === 'Token Expired') {
          let orig = this.origRequest;
          return this.refreshToken().flatMap(res => {
            if(res) {
              let data = res.json();
              if(data.token) {
                return Observable.fromPromise(this.saveToken(data.token));
              } else {
                return Observable.create('');
              }
            }
          }).mergeMap(token => {
            return this.requestWithToken(orig, token);
          });
        }

      case 403:
        this.auth.logout().subscribe(() => {});
      break;  

      default:
        return Observable.throw(err);
    }
  }

  private afterRequest () {

  }

  protected getToken(): Promise<string> {
    return this.storage.get('tokens').then((tokens) => {
      if(!tokens) {
        return null;
      } else {
        return tokens;
      }
    });
  }

  protected saveToken(tokens: any): Promise<string> {
    return this.storage.set('tokens', tokens);
  }

  protected refreshToken(): Observable<Response> {
    return Observable.fromPromise(this.getToken()).mergeMap((tokens: any) => {
      if (!tokens) {
        return Observable.throw('dfds');
      } else {
        return this.post('http://runtracker.ryanknights.co.uk/api/authenticate/refreshToken', {
          token: tokens['refresh']
        }, null, true);
      }
    });
  }  
}