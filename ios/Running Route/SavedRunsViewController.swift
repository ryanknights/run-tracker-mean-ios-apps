//
//  SavedRunsViewController.swift
//  Running Route
//
//  Created by Ryan Knights on 12/05/2016.
//  Copyright © 2016 Ryan Knights. All rights reserved.
//

import UIKit

class SavedRunsViewController: UITableViewController {
    
    var runs = [Run]()
    var runIndexToDelete: Int!
    
    override func viewDidLoad () {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if let existingSavedRuns = SavedRuns.instance.getRuns() {
            runs = existingSavedRuns
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return runs.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SavedRunCell", for: indexPath) as! SavedRunCell
        
        let run = runs[indexPath.row] as Run
        
        cell.run = run
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "saveRunIdentifier",
            let destination = segue.destination as? SavedRunViewController,
            let runIndex = tableView.indexPathForSelectedRow?.row {
    
            destination.run = runs[runIndex]
            destination.runIndex = runIndex
        }
    }
    
    @IBAction func deleteUser (_ segue: UIStoryboardSegue) {
        
        if SavedRuns.instance.deleteRun(runIndexToDelete) {
            
            runs = SavedRuns.instance.getRuns()!
            tableView.reloadData()
        }
    }
}

