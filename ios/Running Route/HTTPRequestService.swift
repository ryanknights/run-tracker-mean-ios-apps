//
//  HTTPRequest.swift
//  Running Route
//
//  Created by Ryan Knights on 03/06/2016.
//  Copyright © 2016 Ryan Knights. All rights reserved.
//

import Foundation
import SwiftyJSON

typealias ServiceResponse = (JSON, HTTPURLResponse?, NSError?) -> Void

class HTTPRequestService: NSObject {
    
    static let sharedInstance = HTTPRequestService()
    
    // MARK: Perform a GET Request
    func makeHTTPGetRequest (_ path: String, useAccessToken: Bool, onCompletion: @escaping ServiceResponse) {
        
        let request = NSMutableURLRequest(url: URL(string: path)!)
        
        if useAccessToken {
            
            request.setValue("Bearer " + CurrentUserService.sharedInstance.getAccessToken()!, forHTTPHeaderField: "Authorization")
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            
            let response = response as? HTTPURLResponse
            
            if let jsonData = data {
                
                var json:JSON = JSON(data: jsonData)
                
                if (json.null != nil) {
                    json = ["response": String(data: data!, encoding: String.Encoding.utf8)!] as JSON
                }
                
                onCompletion(json, response, error as NSError?)
                
            } else {
                
                onCompletion(JSON.null, response, error! as NSError)
            }
        })
        
        task.resume()
    }
    
    //MARK: Perform a POST Request
    func makeHTTPPostRequest (_ path: String, useAccessToken: Bool, body: [String: AnyObject], onCompletion: @escaping ServiceResponse) {
        
        let request = NSMutableURLRequest(url: URL(string: path)!)
        
        request.httpMethod = "POST"
        
        if useAccessToken {
            
            request.setValue("Bearer " + CurrentUserService.sharedInstance.getAccessToken()!, forHTTPHeaderField: "Authorization")
        }
        
        do  {
            
            let jsonBody = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
            
            request.httpBody = jsonBody
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("\(jsonBody.count)", forHTTPHeaderField: "Content-Length")
        
            
            let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                
                let response = response as? HTTPURLResponse
                
                if let jsonData = data {
                    
                    var json:JSON = JSON(data: jsonData)
                    
                    if (json.null != nil) {
                        json = ["response": String(data: data!, encoding: String.Encoding.utf8)!] as JSON
                    }
                    
                    onCompletion(json, response, nil)
                    
                } else {
                    
                    onCompletion(nil, response, error! as NSError)
                }
            })
            
            task.resume()
            
        } catch {
            
            onCompletion(nil, nil, nil)
        }
    }
}

