//
//  Run.swift
//  Running Route
//
//  Created by Ryan Knights on 12/05/2016.
//  Copyright © 2016 Ryan Knights. All rights reserved.
//

import UIKit
import CoreLocation
import HealthKit

class Run: NSObject, NSCoding {
    
    var locations: [CLLocation]
    var meters: Double
    var seconds: Double
    var date: Date!
    
    init (locations: [CLLocation], meters: Double, seconds: Double, date: Date) {
        self.locations = locations
        self.meters = meters
        self.seconds = seconds
        self.date = date
        
        super.init()
    }
    
    required convenience init (coder aDecoder: NSCoder) {
        
        let locations = aDecoder.decodeObject(forKey: "locations") as! [CLLocation]
        let meters = aDecoder.decodeDouble(forKey: "meters")
        let seconds = aDecoder.decodeDouble(forKey: "seconds")
        let date = aDecoder.decodeObject(forKey: "date") as! Date
        
        self.init(locations: locations, meters: meters, seconds: seconds, date: date)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(locations, forKey: "locations")
        aCoder.encode(meters, forKey: "meters")
        aCoder.encode(seconds, forKey: "seconds")
        aCoder.encode(date, forKey: "date")
    }
    
    func returnDistance (_ type: String, decimals: Int = 3) -> Double {
        
        var distance: Double
        
        switch (type) {
            
        case "miles":
            distance = 0.000621371192 * meters
        
        default:
            distance = meters
        }
        
        return Double(round(1000 * distance) / 1000)
    }
    
    func returnTimeParts () -> (Int, Int, Int) {
        
        let intSeconds = Int(seconds)
        
        return (intSeconds / 3600, (intSeconds % 3600) / 60, (intSeconds % 3600) % 60)
    }
    
    func returnTimeString () -> String {
        
        let (h, m, s) = returnTimeParts()
        
        return ("\(h) Hours, \(m) Minutes, \(s) Seconds")
    }
    
    func returnDateString () -> String {
        
        let calendar = Calendar.current
        let components = (calendar as NSCalendar).components([.day, .month, .year], from: date)
        
        return ("\(String(describing: components.day))/\(components.month)/\(components.year)")
    }
}
