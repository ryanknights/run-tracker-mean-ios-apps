//
//  SavedRunViewController.swift
//  Running Route
//
//  Created by Ryan Knights on 13/05/2016.
//  Copyright © 2016 Ryan Knights. All rights reserved.
//

import CoreLocation
import HealthKit
import MapKit

class SavedRunViewController: UIViewController, MKMapViewDelegate {
    
    var run: Run!
    var runIndex: Int!
    var map: ResultMap!
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var paceLabel: UILabel!
    @IBOutlet weak var uploadBtn: UIButton!
    
    override func viewDidLoad () {
        super.viewDidLoad()
        
        addLabelInfo()
        map = ResultMap(map: mapView, locations: run.locations)
    }
    
    func addLabelInfo () {
        
        let timeString = run.returnTimeString()
        timeLabel.text = "Time: " + timeString
        
        let distanceInMiles = run.returnDistance("miles")
        distanceLabel.text = "Distance: \(distanceInMiles) Miles"
        
        let paceUnit = HKUnit.second().unitDivided(by: HKUnit.meter())
        let paceQuantity = HKQuantity(unit: paceUnit, doubleValue: run.seconds / run.meters)
        paceLabel.text = "Pace: " + paceQuantity.description
        
        let dateString = run.returnDateString()
        dateLabel.text = dateString
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "deleteRunIdentifier",
            let destination = segue.destination as? SavedRunsViewController {
            
            destination.runIndexToDelete = runIndex
        }
    }
    
    @IBAction func uploadRun (_ sender: AnyObject?) {
        
        SavedRuns.instance.uploadRun(run, onCompletion: {
            DispatchQueue.main.async(execute: {
                self.uploadBtn.isEnabled = false
                self.uploadBtn.setTitle("Run Uploaded...", for: UIControlState())
            })
            }, onError: {
                print("Error uploading run")
        })
    }
}

