//
//  RegisterViewController.swift
//  Running Route
//
//  Created by Ryan Knights on 03/06/2016.
//  Copyright © 2016 Ryan Knights. All rights reserved.
//

import UIKit
import SwiftyJSON

class RegisterViewController: UIViewController, UITextFieldDelegate {

    fileprivate var errors = [UITextField: String]()
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var feedbackLabel: UILabel!
    
    @IBAction func registerPressed (_ sender: UIButton?) {
        
        resetFeedback()
        
        if !validateFields() {
            return displayErrors();
        }
        
        let registerData = ["username": usernameField.text!, "email": emailField.text!, "password": passwordField.text!]
        
        AuthService.sharedInstance.register(registerData as NSDictionary, onCompletion: { (json: JSON, response: HTTPURLResponse?, error: NSError?) in
            
            DispatchQueue.main.async(execute: {
                let loginData = ["username": self.usernameField.text!, "password": self.passwordField.text!]
                AuthService.sharedInstance.login(loginData as NSDictionary, onCompletion: { (json: JSON, response: HTTPURLResponse?, error: NSError?) in
                    DispatchQueue.main.async(execute: {
                        self.moveToHomeView()
                    })
                
                }, onError: { (message: String, response: HTTPURLResponse?, error: NSError?) in
                    DispatchQueue.main.async(execute: {
                        self.feedbackLabel.text = message
                    })
            
                })
            })
        }, onError: { (message: String, response: HTTPURLResponse?, error: NSError?) in
            DispatchQueue.main.async(execute: {
                self.feedbackLabel.text = message
            })
        })
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.usernameField.delegate = self
        self.passwordField.delegate = self
        self.emailField.delegate = self
    }
    
    func validateFields () -> Bool {
        
        if usernameField.text == "" {
            errors[usernameField] = "Enter a username"
        }
        
        if emailField.text == "" {
            errors[emailField] = "Enter an email address"
        }
        
        if passwordField.text == "" {
            errors[passwordField] = "Enter a password"
        }
        
        return errors.isEmpty
    }
    
    func displayErrors () {
        
        guard !errors.isEmpty else {
            return
        }
        
        var feedbackMessage = "Please complete the following errors:\n";
        
        for (_, error) in errors {
            
            feedbackMessage += error + "\n"
        }
        
        feedbackLabel.text = feedbackMessage
    }
    
    func resetFeedback () {
        
        feedbackLabel.text = ""
        errors.removeAll()
    }
    
    func moveToHomeView () {
        
        let homeNavigationController = (self.storyboard?.instantiateViewController(withIdentifier: "HomeNavigationController"))! as UIViewController
        self.present(homeNavigationController, animated:true, completion:nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
}


