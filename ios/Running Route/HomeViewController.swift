//
//  HomeViewController.swift
//  Running Route
//
//  Created by Ryan Knights on 11/05/2016.
//  Copyright © 2016 Ryan Knights. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBAction func resetRuns (_ sender: UIButton) {
        
        let userDefaults = UserDefaults.standard
        
        userDefaults.removeObject(forKey: "runs")
    }
    
    @IBAction func logout (_ sender: UIButton) {
        CurrentUserService.sharedInstance.logout({ () in
            let loginNavigationController = (self.storyboard?.instantiateViewController(withIdentifier: "LoginNavigationController"))! as UIViewController
            self.present(loginNavigationController, animated:true, completion:nil)
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
}
