//
//  SavedRunCell.swift
//  Running Route
//
//  Created by Ryan Knights on 13/05/2016.
//  Copyright © 2016 Ryan Knights. All rights reserved.
//

import UIKit

class SavedRunCell: UITableViewCell {
    
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    var run: Run! {
        didSet {
            let distanceInMiles = run.returnDistance("miles")
            distanceLabel.text = "Distance: \(distanceInMiles) Miles"
            
            let timeString = run.returnTimeString()
            timeLabel.text = "Time: " + timeString
            
            let dateString = run.returnDateString()
            dateLabel.text = dateString
        }
    }
}
