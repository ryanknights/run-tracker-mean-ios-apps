//
//  RunResultsViewController.swift
//  Running Route
//
//  Created by Ryan Knights on 11/05/2016.
//  Copyright © 2016 Ryan Knights. All rights reserved.
//

import UIKit
import CoreLocation
import HealthKit
import MapKit

class RunResultsViewController: UIViewController, MKMapViewDelegate {
    
    var run: Run!
    var map: ResultMap!
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var paceLabel: UILabel!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var uploadBtn: UIButton!
    
    override func viewDidLoad () {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        addLabelInfo()
        
        map = ResultMap(map: mapView, locations: run.locations)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addLabelInfo () {
        
        let timeString = run.returnTimeString()
        timeLabel.text = "Time: " + timeString
        
        let distanceInMiles = run.returnDistance("miles")
        distanceLabel.text = "Distance: \(distanceInMiles) Miles"
        
        let paceUnit = HKUnit.second().unitDivided(by: HKUnit.meter())
        let paceQuantity = HKQuantity(unit: paceUnit, doubleValue: run.seconds / run.meters)
        paceLabel.text = "Pace: " + paceQuantity.description
    }
    
    @IBAction func saveRun (_ sender: AnyObject?) {
        
        if SavedRuns.instance.saveRun(run) {
            saveBtn.isEnabled = false
            saveBtn.setTitle("Run Saved...", for: UIControlState())
        }
    }
    
    @IBAction func uploadRun (_ sender: AnyObject?) {
        
        SavedRuns.instance.uploadRun(run, onCompletion: {
            DispatchQueue.main.async(execute: {
                self.uploadBtn.isEnabled = false
                self.uploadBtn.setTitle("Run Uploaded...", for: UIControlState())
            })
        }, onError: {
            print("Error uploading run")
        })
    }
    
    @IBAction func finishRun (_ sender: AnyObject?) {
        
        let homeNavigationController = (self.storyboard?.instantiateViewController(withIdentifier: "HomeNavigationController"))! as UIViewController
        self.present(homeNavigationController, animated:true, completion:nil)
    }
}
