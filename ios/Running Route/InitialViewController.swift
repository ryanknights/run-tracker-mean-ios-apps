//
//  InitialViewController.swift
//  Running Route
//
//  Created by Ryan Knights on 03/06/2016.
//  Copyright © 2016 Ryan Knights. All rights reserved.
//

import UIKit
import SwiftyJSON

class InitialViewController: UIViewController {
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        guard CurrentUserService.sharedInstance.isLoggedIn() else {
            return performSegue(withIdentifier: "InitialToLogin", sender: nil)
        }
        
        authenticateUser()
    }
    
    func authenticateUser () {
        
        AuthService.sharedInstance.authenticate({ (json: JSON, response: HTTPURLResponse?, error: NSError?) in

            let userData = ["username": json["user"]["username"].stringValue, "userid": json["user"]["userid"].stringValue]
            CurrentUserService.sharedInstance.setUser(userData as NSDictionary)
            self.performSegue(withIdentifier: "InitialToHome", sender: nil)
            
        }, onAuthenticateError: { (json: JSON, response: HTTPURLResponse?, error: NSError?) in

            CurrentUserService.sharedInstance.destroyUser()
            return self.performSegue(withIdentifier: "InitialToLogin", sender: nil)
        })
    }
}

