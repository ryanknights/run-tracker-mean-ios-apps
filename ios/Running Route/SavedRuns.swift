//
//  SavedRuns.swift
//  Running Route
//
//  Created by Ryan Knights on 13/05/2016.
//  Copyright © 2016 Ryan Knights. All rights reserved.
//

import UIKit
import SwiftyJSON

class SavedRuns {
    
    static let instance = SavedRuns()
    
    let userDefaults = UserDefaults.standard
    let baseURL = "http://runtracker.ryanknights.co.uk/api"
    
    func getRuns () -> [Run]? {
        
        guard let userID = CurrentUserService.sharedInstance.user!["userid"] else {
            return nil
        }
        
        let key = String(describing: userID) + "_runs"
        
        if let decoded = userDefaults.object(forKey: key) as! Data? {
            return NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [Run]
        } else {
            return nil
        }
    }
    
    func saveRun (_ run: Run) -> Bool {

        guard let userID = CurrentUserService.sharedInstance.user!["userid"] else {
            return false
        }

        let key = String(describing: userID) + "_runs"
        
        var savedRuns = [Run]()
    
        if let decoded = userDefaults.object(forKey: key) as! Data? {
            savedRuns = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [Run]
        }
        
        savedRuns.append(run)
        
        let encodedSavedRuns = NSKeyedArchiver.archivedData(withRootObject: savedRuns)
        userDefaults.set(encodedSavedRuns, forKey: key)
        userDefaults.synchronize()
        
        return true
    }
    
    func uploadRun (_ run: Run, onCompletion: @escaping () -> Void, onError: @escaping () -> Void) {
        
        let route = baseURL + "/runs"
        
        var runData = [String: AnyObject]()
        
        runData["meters"] = run.meters as AnyObject
        runData["seconds"] = run.seconds as AnyObject
        runData["date"] = run.date.description as AnyObject
        
        var runLocations: [AnyObject] = []
        for location in run.locations {
            var data = [String: Any]()
            
            data["location"] = ["lat": location.coordinate.latitude, "lng": location.coordinate.longitude]
            data["time"] = location.timestamp.description as AnyObject
            data["description"] = location.description as AnyObject
            
            runLocations.append(data as AnyObject)
        }
        
        runData["locations"] = runLocations as AnyObject

        HTTPRequestService.sharedInstance.makeHTTPPostRequest(route, useAccessToken: true, body: runData , onCompletion: { json, response, err in
            
            guard let httpResponse = response else {
                return onError()
            }
            
            print(httpResponse.statusCode)
            print(json["response"].stringValue)
            
            if httpResponse.statusCode == 200 {
                
                return onCompletion()
                
            } else {
                
                guard httpResponse.statusCode == 401 && json["response"].stringValue == "Token Expired" else {
                    return onError()
                }
                
                print("Needs to refresh token")
                
                AuthService.sharedInstance.refreshToken(CurrentUserService.sharedInstance.getRefreshToken(), onCompletion: { (json: JSON, response: HTTPURLResponse?, error: NSError?) in
                    
                    return self.uploadRun(run, onCompletion: onCompletion, onError: onError)
                    
                    }, onError: { (json: JSON, response: HTTPURLResponse?, error: NSError?) in
                        
                        return onError()
                })
            }
        })
    }
    
    func deleteRun (_ indexToDelete: Int) -> Bool {
        
        guard let userID = CurrentUserService.sharedInstance.user!["userid"] else {
            return false
        }
        
        let key = String(describing: userID) + "_runs"
        
        var savedRuns = [Run]()
        
        if let decoded = userDefaults.object(forKey: key) as! Data? {
            savedRuns = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [Run]
        }
        
        savedRuns.remove(at: indexToDelete)

        let encodedSavedRuns = NSKeyedArchiver.archivedData(withRootObject: savedRuns)
        userDefaults.set(encodedSavedRuns, forKey: key)
        userDefaults.synchronize()
        
        return true
    }
}
