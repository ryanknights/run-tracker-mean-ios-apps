//
//  UserService.swift
//  Running Route
//
//  Created by Ryan Knights on 03/06/2016.
//  Copyright © 2016 Ryan Knights. All rights reserved.
//

import Foundation
import SwiftyJSON
import SwiftKeychainWrapper

class CurrentUserService {
    
    static let sharedInstance = CurrentUserService()
    
    var user: NSDictionary?
    
    func setUser (_ userData:NSDictionary) {

        UserDefaults.standard.set(true, forKey: "isUserLoggedIn")
        user = userData
    }
    
    func getUser () -> NSDictionary? {
        
        if (user != nil) {
            return user
        } else {
            return nil
        }
    }
    
    func destroyUser () {
        
        user = nil
        UserDefaults.standard.set(false, forKey: "isUserLoggedIn")
    }
    
    func getAccessToken () -> String? {
        
        let accessToken: String? = KeychainWrapper.standard.string(forKey: "accessToken")
        return accessToken
    }
    
    func setAccessToken (_ token: String) -> Bool {
        
        let saveSuccessful: Bool = KeychainWrapper.standard.set(token, forKey: "accessToken")
        return saveSuccessful
    }
    
    func destroyAccessToken () -> Bool {
        
        let removeSuccessful: Bool = KeychainWrapper.standard.removeObject(forKey: "accessToken")
        return removeSuccessful
    }
    
    func getRefreshToken () -> String? {
        
        let refreshToken: String? = KeychainWrapper.standard.string(forKey: "refreshToken")
        return refreshToken
    }
    
    func setRefreshToken (_ token: String) -> Bool {
        
        let saveSuccessful: Bool = KeychainWrapper.standard.set(token, forKey: "refreshToken")
        return saveSuccessful
    }
    
    func destroyRefreshToken () -> Bool {
        
        let removeSuccessful: Bool = KeychainWrapper.standard.removeObject(forKey: "refreshToken")
        return removeSuccessful
    }
    
    func isLoggedIn () -> Bool {

        return (UserDefaults.standard.object(forKey: "isUserLoggedIn") as! Bool? == true)
    }
    
    func logout (_ onCompletion: () -> Void) {
        destroyUser()
        destroyAccessToken()
        destroyRefreshToken()
        onCompletion()
    }
}


