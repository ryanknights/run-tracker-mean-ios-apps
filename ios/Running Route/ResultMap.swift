//
//  ResultMap.swift
//  Running Route
//
//  Created by Ryan Knights on 12/05/2016.
//  Copyright © 2016 Ryan Knights. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation

class ResultMap: NSObject, MKMapViewDelegate {
    
    var map: MKMapView
    var locations: [CLLocation]
    
    init (map: MKMapView, locations: [CLLocation]) {
        
        self.map = map
        self.locations = locations
        
        super.init()
    
        loadMap()
    }
    
    func mapRegion () -> MKCoordinateRegion {
        
        let initialLoc = locations.first
        
        var minLat = initialLoc!.coordinate.latitude
        var minLng = initialLoc!.coordinate.longitude
        var maxLat = minLat
        var maxLng = minLng
        
        for location in locations {
            minLat = min(minLat, location.coordinate.latitude)
            minLng = min(minLng, location.coordinate.longitude)
            maxLat = max(maxLat, location.coordinate.latitude)
            maxLng = max(maxLng, location.coordinate.longitude)
        }
        
        return MKCoordinateRegion(
            center: CLLocationCoordinate2D(latitude: (minLat + maxLat) / 2, longitude: (minLng + maxLng) / 2),
            span: MKCoordinateSpan(latitudeDelta: (maxLat - minLat) * 1.1, longitudeDelta: (maxLng - minLng) * 1.1)
        )
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        if overlay is MKPolyline {
            let polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = UIColor.black
            polylineRenderer.lineWidth = 5
            return polylineRenderer
        }
        
        return MKPolylineRenderer()
    }
    
    func polyline () -> MKPolyline {
        
        var coords = [CLLocationCoordinate2D]()
        
        for location in locations {
            let coord = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            coords.append(coord)
        }
        
        return MKPolyline(coordinates: &coords, count: locations.count)
    }
    
    func loadMap () {
        
        map.delegate = self
        
        if locations.count > 0 {
            
            map.region = mapRegion()
            map.add(polyline())
            
        }
    }
    
    func updatePolyline (_ fromLocation: CLLocation, toLocation: CLLocation) {
        
        var coords = [CLLocationCoordinate2D]()
        
        coords.append(fromLocation.coordinate)
        coords.append(toLocation.coordinate)
        
        let region = MKCoordinateRegionMakeWithDistance(toLocation.coordinate, 500, 500)
        map.setRegion(region, animated: true)
        
        map.add(MKPolyline(coordinates: &coords, count: coords.count))
    }
    
}
