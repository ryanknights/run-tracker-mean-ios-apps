//
//  LoginViewController.swift
//  Running Route
//
//  Created by Ryan Knights on 03/06/2016.
//  Copyright © 2016 Ryan Knights. All rights reserved.
//

import UIKit
import SwiftyJSON

class LoginViewController: UIViewController, UITextFieldDelegate {

    fileprivate var errors = [UITextField: String]()
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var feedbackLabel: UILabel!
    
    @IBAction func resetUserPressed (_ sender: UIButton?) {
        CurrentUserService.sharedInstance.destroyUser()
        CurrentUserService.sharedInstance.destroyAccessToken()
        CurrentUserService.sharedInstance.destroyRefreshToken()
    }
    
    @IBAction func loginPressed (_ sender: UIButton?) {
        
        feedbackLabel.text = "Logging in..."
        
        let loginData = ["username": usernameField.text!, "password": passwordField.text!]
        
        AuthService.sharedInstance.login(loginData as NSDictionary, onCompletion: { (json: JSON, response: HTTPURLResponse?, error: NSError?) in
            
            DispatchQueue.main.async(execute: {
                self.moveToHomeView()
            })
            
            }, onError: { (message: String, response: HTTPURLResponse?, error: NSError?) in
                
                DispatchQueue.main.async(execute: {
                    self.feedbackLabel.text = message
                })
            })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.usernameField.delegate = self
        self.passwordField.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func validateFields () -> Bool {
        
        if usernameField.text == "" {
            errors[usernameField] = "Enter a username"
        }
        
        if passwordField.text == "" {
            errors[passwordField] = "Enter a password"
        }
        
        return errors.isEmpty
    }
    
    func displayErrors () {
        
        guard !errors.isEmpty else {
            return
        }
        
        var feedbackMessage = "Please complete the following errors:\n";
        
        for (_, error) in errors {
            
            feedbackMessage += error + "\n"
        }
        
        feedbackLabel.text = feedbackMessage
    }
    
    func resetFeedback () {
        
        feedbackLabel.text = ""
        errors.removeAll()
    }
    
    func moveToHomeView () {

        let homeNavigationController = (self.storyboard?.instantiateViewController(withIdentifier: "HomeNavigationController"))! as UIViewController
        self.present(homeNavigationController, animated:true, completion:nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
}

