//
//  AuthService.swift
//  Running Route
//
//  Created by Ryan Knights on 03/06/2016.
//  Copyright © 2016 Ryan Knights. All rights reserved.
//

import Foundation
import SwiftyJSON

class AuthService {
    
    static let sharedInstance = AuthService()
    
    let baseURL = "http://runtracker.ryanknights.co.uk/api"
    
    func register (_ data: NSDictionary, onCompletion: @escaping (JSON, HTTPURLResponse?, NSError?) -> Void, onError: @escaping (String, HTTPURLResponse?, NSError?) -> Void) {
        
        let route = baseURL + "/register"
        
        HTTPRequestService.sharedInstance.makeHTTPPostRequest(route, useAccessToken: false, body: data as! [String : AnyObject], onCompletion: { json, response, err in
            
            guard let httpResponse = response else {
                return onError("There was a problem making the request, please try again.", response, err)
            }
            
            let statusCode = httpResponse.statusCode
            
            if (statusCode == 200) {
                onCompletion(json as JSON, response, err)
            } else {
                onError(json["response"].stringValue, response, err)
            }
        })
    }
    
    func login (_ data: NSDictionary, onCompletion: @escaping (JSON, HTTPURLResponse?, NSError?) -> Void, onError: @escaping (String, HTTPURLResponse?, NSError?) -> Void) {
        
        let route = baseURL + "/login"
        
        HTTPRequestService.sharedInstance.makeHTTPPostRequest(route, useAccessToken: false, body: data as! [String : AnyObject], onCompletion: { json, response, err in
            
            guard let httpResponse = response else {
                return onError("There was a problem making the request, please try again.", response, err)
            }
            
            let statusCode = httpResponse.statusCode
            
            if (statusCode == 200) {
                
                let userData = ["username": json["user"]["username"].stringValue, "userid": json["user"]["userid"].stringValue]
                let accessToken = json["token"]["access"].stringValue
                let refreshToken = json["token"]["refresh"].stringValue
                
                CurrentUserService.sharedInstance.setUser(userData as NSDictionary)
                CurrentUserService.sharedInstance.setAccessToken(accessToken)
                CurrentUserService.sharedInstance.setRefreshToken(refreshToken)
                
                onCompletion(json, response, err)
                
            } else {
                return onError("Incorrect Username/Password", response, err)
            }
        })
    }
    
    func authenticate (_ onAuthenticateCompletion: @escaping (JSON, HTTPURLResponse?, NSError?) -> Void, onAuthenticateError: @escaping (JSON, HTTPURLResponse?, NSError?) -> Void) {
        
        let route = baseURL + "/authenticate"
        
        HTTPRequestService.sharedInstance.makeHTTPGetRequest(route, useAccessToken: true, onCompletion: { json, response, err in
            
            guard let httpResponse = response else {
                return onAuthenticateError(json as JSON, response, err)
            }
            
            let statusCode = httpResponse.statusCode
            
            if (statusCode == 200) {
                
               onAuthenticateCompletion(json as JSON, response, err)
                
            } else {
                
                guard statusCode == 401 && json["response"].stringValue == "Token Expired" else {
                    return onAuthenticateError(json as JSON, response, err)
                }
                
                AuthService.sharedInstance.refreshToken(CurrentUserService.sharedInstance.getRefreshToken(), onCompletion: { (json: JSON, response: HTTPURLResponse?, error: NSError?) in
                    
                    self.authenticate(onAuthenticateCompletion, onAuthenticateError: onAuthenticateError)
                    
                    }, onError: { (json: JSON, response: HTTPURLResponse?, error: NSError?) in
                        
                    onAuthenticateError(json as JSON, response, err)
                })
            }
        })
    }
    
    func refreshToken (_ refreshToken: String?, onCompletion: @escaping (JSON, HTTPURLResponse?, NSError?) -> Void, onError: @escaping (JSON, HTTPURLResponse?, NSError?) -> Void) {
        
        let route = baseURL + "/authenticate/refreshToken"
        
        var data = [String: AnyObject]()
        
        if let refreshToken = refreshToken {
            data["token"] = refreshToken as AnyObject
        }
        
        HTTPRequestService.sharedInstance.makeHTTPPostRequest(route, useAccessToken: false, body: data, onCompletion: { json, response, err in
            
            guard let httpResponse = response else {
                return onError(json as JSON, response, err)
            }
            
            let statusCode = httpResponse.statusCode
            
            if (statusCode == 200) {
                
                let accessToken = json["token"]["access"].stringValue
                let refreshToken = json["token"]["refresh"].stringValue
                
                CurrentUserService.sharedInstance.setAccessToken(accessToken)
                CurrentUserService.sharedInstance.setRefreshToken(refreshToken)
                
                onCompletion(json as JSON, response, err)
                
            } else {
                
                onError(json as JSON, response, err)
            }
        })
    }
}

