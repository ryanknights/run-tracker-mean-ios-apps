//
//  NewRunViewController.swift
//  Running Route
//
//  Created by Ryan Knights on 11/05/2016.
//  Copyright © 2016 Ryan Knights. All rights reserved.
//

import UIKit
import CoreLocation
import HealthKit
import MapKit

class NewRunViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var startBtn: UIButton!
    @IBOutlet weak var stopBtn: UIButton!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var paceLabel: UILabel!
    @IBOutlet weak var promptLabel: UILabel!
    
    var run = Run(locations: [CLLocation](), meters: 0.0, seconds: 0.0, date: Date())
    var map: ResultMap!
    
    lazy var locationManager: CLLocationManager = {
        var _locationManager = CLLocationManager()
        _locationManager.delegate = self
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest
        _locationManager.activityType = .fitness
        _locationManager.distanceFilter = 10.0
        _locationManager.pausesLocationUpdatesAutomatically = false
        
        if _locationManager.responds(to: #selector(getter: CLLocationManager.allowsBackgroundLocationUpdates)) {
            _locationManager.allowsBackgroundLocationUpdates = true
        }
        
        return _locationManager
    }()

    lazy var timer = Timer()
    
    var locationBacklog = [CLLocation]()
    
    var startTime: TimeInterval!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
 
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(NewRunViewController.applicationDidBecomeActive(_:)),
            name: NSNotification.Name.UIApplicationDidBecomeActive,
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(NewRunViewController.applicationWillResignActive(_:)),
            name: NSNotification.Name.UIApplicationWillResignActive,
            object: nil)
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func applicationDidBecomeActive(_ notification: Notification) {
    
        guard !locationBacklog.isEmpty else {
            return
        }
        
        locationManager.stopUpdatingLocation()
        
        for location in locationBacklog {
            
            addLocation(location)
        }
        
        locationBacklog.removeAll(keepingCapacity: false)
        
        startTimer()
        startLocationUpdates()
    }
    
    func applicationWillResignActive(_ notification: Notification) {
        stopTimer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        locationManager.requestAlwaysAuthorization()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopTimer()
    }
    
    func eachSecond(_ timer: Timer) {
        
        let currentTime = Date.timeIntervalSinceReferenceDate
        let elapsedTime: TimeInterval = currentTime - startTime
        
        run.seconds = elapsedTime
        
        let timeString = run.returnTimeString()
        timeLabel.text = "Time: " + timeString
        
        let distanceInMiles = run.returnDistance("miles")
        distanceLabel.text = "Distance: \(distanceInMiles) Miles"
        
        let paceUnit = HKUnit.second().unitDivided(by: HKUnit.meter())
        let paceQuantity = HKQuantity(unit: paceUnit, doubleValue: run.seconds / run.meters)
        paceLabel.text = "Pace: " + paceQuantity.description
    }
    
    func startTimer () {
        timer = Timer.scheduledTimer(
            timeInterval: 1,
            target: self,
            selector: #selector(NewRunViewController.eachSecond(_:)),
            userInfo: nil,
            repeats: true
        )
    }
    
    func stopTimer () {
        timer.invalidate()
    }
    
    func startLocationUpdates () {
        locationManager.startUpdatingLocation()
    }
    
    func stopTracking () {
        stopTimer()
        locationManager.stopUpdatingLocation()
    }
    
    @IBAction func startNewRun (_ Sender: AnyObject!) {
        
        startBtn.isHidden = true
        stopBtn.isHidden = false
        
        distanceLabel.isHidden = false
        timeLabel.isHidden = false
        paceLabel.isHidden = false
        mapView.isHidden = false
        promptLabel.text = "Tracking..."
        
        run.seconds = 0.0
        run.meters = 0.0
        run.locations.removeAll(keepingCapacity: false)
        startTime = Date.timeIntervalSinceReferenceDate
        
        map = ResultMap(map: mapView, locations: run.locations)
        
        startTimer()
        startLocationUpdates()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let runResultsViewController = (segue.destination as! UINavigationController).topViewController as! RunResultsViewController
        
        runResultsViewController.run = run
        stopTracking()
    }
    
    func addLocation (_ location: CLLocation) {
        if run.locations.count > 0 {
            run.meters += location.distance(from: self.run.locations.last!)
            map.updatePolyline(self.run.locations.last!, toLocation: location)
        }
        
        run.locations.append(location)
    }
}

// MARK: - CLLocationManagerDelegation
extension NewRunViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        for location in locations {
            if location.horizontalAccuracy < 20 {
                
                if UIApplication.shared.applicationState == .active {
                    addLocation(location)
                } else {
                    locationBacklog.append(location)
                }
            }
        }
    }}
